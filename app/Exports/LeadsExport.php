<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class LeadsExport implements FromView
{
    public function view(): View
    {
        // $data = DB::table('users')
        //             ->leftJoin('activities', 'activities.user_id', 'users.id')
        //             ->leftJoin('cms_users', 'cms_users.id','users.admin_id')
        //             ->select('users.name', 'users.email', 'users.phone', 'users.status', 'activities.activity', 'activities.next_fu', 'activities.created_at', 'activities.updated_at', 'cms_users.name as assign_by', 'activities.user_id')
        //             // ->where('users.status', 'lead')
        //             ->orderBy('activities.updated_at', 'desc')
        //             ->get()->unique('user_id');

        $data = DB::table('users')
                    ->leftJoin('activities', 'activities.user_id', 'users.id')
                    ->leftJoin('cms_users', 'cms_users.id','users.admin_id')
                    ->select('users.name', 'users.email', 'users.phone', 'users.status', 'activities.activity', 'activities.next_fu', 'activities.created_at', 'activities.updated_at', 'cms_users.name as assign_by', 'activities.user_id', 'users.id')
                    // ->where('users.status', 'lead')
                    ->orderBy('activities.updated_at', 'desc')
                    ->get()->unique('user_id');

        $not = DB::table('users')
        ->get();

        $tempUser = [];
        foreach ($not as $n) {
            $a = DB::table('activities')->where('user_id', $n->id)->first();

            if (empty($a)) {
                $temp = [
                    'id' => $n->id,
                    'name' => $n->name,
                    'email' => $n->email,
                    'phone' => $n->phone,
                    'status' => $n->status,
                    'activity' => null,
                    'next_fu' => null,
                    'created_at' => null,
                    'updated_at' => null,
                    'assign_by' => $n->assign_by,
                ];

                $tempUser[] = $temp;
            }
        }
        // dd($data);

        return view('excel.lead', [
            'data' => $data,
            'tempUser' => $tempUser
        ]);
    }
}
