<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class ContactUsController extends Controller
{
    public function create(Request $request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $agent = new Agent();
        
        $request->validate([
            'contact_name' => 'required|min:2',
            'contact_email' => 'required|email',
        ]);

        if ($agent->isPhone()) {
            $screen = "Phone";
        } elseif ($agent->isTablet()) {
            $screen = "Tablet";
        } elseif ($agent->isDesktop()) {
            $screen = "Desktop";
        }

        $data = [
            'name' => $request->contact_name,
            'email' => $request->contact_email,
            'phone' => $request->contact_whatsapp,
            'status' => 'lead',
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'screen' => $screen,
            'version_platform' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'version_browser' =>  $agent->version($agent->browser()),
            'languages' => $agent->languages()[0],
            'created_at' =>  Carbon::now(),
        ];

        $insertUser = DB::table('users')->insertGetId($data);


        $contact = DB::table('contact_us')->insertGetId([
            'user_id' => $insertUser,
            'created_at' =>  Carbon::now(),
            'company' =>$request->contact_company,
            'message'=> $request->contact_message,
        ]);

        return redirect()->back()->with('message', 'berhasil');
    }
}
