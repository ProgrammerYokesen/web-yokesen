<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function getIndex(){
        // meet hari ini dan nanti
        $dateNow = Carbon::now();
        $meetCount = DB::table('booking_meet')->whereDate('tgl', '>=', $dateNow)->count();
        $leadCount = DB::table('users')->where('status', 'lead')->count();
        $contactCount = DB::table('users')->where('status', 'contact')->count();
        $winCount = DB::table('users')->where('status', 'win')->count();
        $loseCount = DB::table('users')->where('status', 'lose')->count();

        $activities = DB::table('activities')->join('users', 'users.id', 'activities.user_id')
        ->join('cms_users', 'cms_users.id', 'activities.admin_id')
        ->select('users.name as user_name', 'cms_users.name as admin_name', 'activity')
        ->orderBy('activities.id', 'desc')->paginate(12);

        $visitor = DB::table('visits')->select('landing_page', 'landing_domain')
        ->orderBy('id', 'desc')->paginate(12);
        // return response()->json($activities);
       return view('admin.dashboard', [
           'meetCount' => $meetCount,
           'leadCount' => $leadCount,
           'contactCount' => $contactCount,
           'winCount' => $winCount,
           'loseCount' => $loseCount,
           'activities'=> $activities,
           'visitors' => $visitor
       ]); 
    }
}
