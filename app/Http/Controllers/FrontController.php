<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function index(){
        $header = DB::table('headers')->orderBy('id', 'desc')->first();
        // dd($header);
        $about = DB::table('abouts')->orderBy('id', 'desc')->first();
        // dd($about);
        return view('layouts.front', [
            'head' => $header,
            'about' => $about
        ]);
    }
}
