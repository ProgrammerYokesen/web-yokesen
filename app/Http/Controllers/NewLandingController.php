<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\GoogleCalendar\Event;
class NewLandingController extends Controller
{
    //
    public function eventLP(){
        return view('pages.event-lp.event');
    }
    public function createMeet(Request $request){
       $capcha = $request->input('g-recaptcha-response');
       if ($capcha == null) {
          return redirect()->back()->with('errorCapcha', 'errorCapcha');
       }
       $data = $request->all();
       $dateTimeString = $request->date." ".$request->time.":00";
       $dueDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $dateTimeString, 'Asia/Jakarta');
       // dd($request->all());
       $event = new Event;
       // FOR ADD EVENT IN GOOGLE CALENDAR
       $event->name = 'Request Bertemu AE oleh '.$request->name;
       $event->description = 'Hubungi untuk contact langsung:'.$request->input('landing-enquiry-idd').$request->phone ;
       $event->startDateTime = $dueDateTime;
       $event->endDateTime = $dueDateTime->addHour(2);
       $event->save();
        $insertMeet = DB::table('booking_meet')->insertGetId([
            'name' => $request->name,
            'email' => $request->email,
            'company' =>$request->company,
            'phone' => $request->whatsapp,
            'tgl' => $request->date,
            'jam' => $request->time
        ]);

        return redirect()->back()->with('messageSuccess', 'berhasil');
    }
}
