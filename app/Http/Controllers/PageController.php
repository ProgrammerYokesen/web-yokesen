<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function homePage()
    {
        return view('pages.homepage');
    }

    public function aboutPage()
    {
        return view('pages.about');
    }

    public function digitalPage()
    {
        return view('pages.digital');
    }

    public function thisThatPage()
    {
        return view('pages.thattothis');
    }

    public function contactPage()
    {
        return view('pages.contact');
    }
    
    public function sitemapPage()
    {
        return view('pages.sitemap');
    }
    
    public function sitemapXMLPage()
    {
        return response()->view('pages.xml-sitemap')->header('Content-Type', 'text/xml'); 
    }

    public function teraturLandingPage()
    {
        return view('pages.landing-page.tcw');
    }

    public function portoPage(){
        return view('pages.portofolio');
    }
    public function gamificationPage(){
        return view('pages.gamification');
    }
    public function webinarPage(){
        return view('pages.webinar');
    }
}
