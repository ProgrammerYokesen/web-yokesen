<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $agent = new Agent();

        $request->validate([
            'name' => 'required|min:2',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|numeric|unique:users,phone'
        ],[
            'name.required' => 'Nama tidak boleh kosong',
            'name.min' => 'Nama minimal 2 karakter',
            'email.required' => 'Email tidak boleh kosong',
            'email.email' => 'Format email tidak valid',
            'email.unique' => 'Email sudah terdaftar',
            'phone.required' => 'No. Whatsapp tidak boleh kosong',
            'phone.numeric' => 'No. Whatsapp harus terdiri dari angka',
            'phone.unique' => 'No. Whatsapp sudah terdaftar'
        ]);

        if ($agent->isPhone()) {
            $screen = "Phone";
        } elseif ($agent->isTablet()) {
            $screen = "Tablet";
        } elseif ($agent->isDesktop()) {
            $screen = "Desktop";
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => 'lead',
            'ip_address' => \Request::ip(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'screen' => $screen,
            'version_platform' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'version_browser' =>  $agent->version($agent->browser()),
            'languages' => $agent->languages(),
            'created_at' =>  Carbon::now(),
        ];

        $userId = DB::table('users')->insertGetId($data);

        $cookie = $_COOKIE['footprints'];
        // dd($cookie);
        if($cookie != NULL){
            // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);
            // $decryptedString = $encrypter->decrypt($cookie);
            $decryptedString = \Crypt::decrypt($cookie, false);
            // dd($decryptedString);
            // $cookies = explode("|",$decryptedString);

            $visits = DB::table('visits')->where('cookie_token', $decryptedString)->update([
                'user_id' => $userId
            ]);
        }

        return back()->with('status', 'Registrasi Berhasil');
    }
}
