<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CRUDBooster;
use DB;

class UserController extends Controller
{
    public function userDetail($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        $activities = DB::table('activities')->join('users', 'users.id', 'activities.user_id')->where('activities.user_id', $id)->select('users.name', 'activities.activity', 'activities.next_fu','activities.created_at','activities.updated_at')->orderBy('activities.created_at', 'desc')->get();
        $nextFu = $activities[0]->next_fu;
        $users = DB::table('cms_users')->get();

        return view('admin.user-detail', [
            'user' => $user,
            'activities' => $activities,
            'next_fu' => $nextFu,
            'users' => $users
        ]);
    }

    public function updateStatus(Request $request, $id)
    {
        $data = [
            'status' => $request->status,
            'admin_id' => CRUDBooster::myId()
        ];
        $user = DB::table('users')->where('id', $id)->update($data);

        if ($request->status == 'lead') {
            return redirect()->route('userLeadDetail', [$id]);
        } else if ($request->status == 'contact') {
            return redirect()->route('userContactDetail', [$id]);
        } else if ($request->status == 'potential') {
            return redirect()->route('userPotentialDetail', [$id]);
        } else if ($request->status == 'win') {
            return redirect()->route('userWinDetail', [$id]);
        } else {
            return redirect()->route('userLoseDetail', [$id]);
        }
    }
}
