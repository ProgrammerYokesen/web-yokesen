<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class blogController extends Controller
{
    public function blogPage()
    {
        $data['blogs'] = DB::table('blogs')
            ->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')
            ->select('blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogLike', 'blogs.blogRead', 'blogs.created_at', 'blogs.blogImage1', 'blogs.id', 'blogs.titleImage')
            ->orderBy('created_at', 'desc')
            ->paginate(9);
        $data['latest'] = DB::table('blogs')
            ->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')
            ->select('blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogLike', 'blogs.blogRead', 'blogs.created_at', 'blogs.blogImage1', 'blogs.id', 'blogs.titleImage')
            ->orderBy('created_at', 'desc')
            ->limit(6)->get();
        // dd($data);

        return view('pages.blogs.home', $data);
    }

    public function detailBlogPage($slug)
    {
        $data['blog'] = DB::table('blogs')
            ->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')
            ->where('blogSlug', $slug)->first();

        $data['blogs'] = DB::table('blogs')
            ->join('cms_users', 'cms_users.id', 'blogs.blogAuthor')
            ->select('blogs.blogTitle', 'blogs.blogSlug', 'blogs.blogContent', 'blogs.blogLike', 'blogs.blogRead', 'blogs.created_at', 'blogs.blogImage1', 'blogs.id', 'blogs.titleImage')
            ->where('blogs.blogSlug', "!=", $slug)
            ->orderBy('blogs.created_at', 'desc')
            ->take(6)->get();

        // dd($data);

        return view('pages.blogs.detail-blog', $data);
    }

    // public function loadMoreComment(Request $request)
    // {

    //     $data = DB::table('blog_comments')
    //         ->select('blog_comments.*', 'users.name', 'users.avatars')
    //         ->join('users', 'users.id', 'blog_comments.user_id')
    //         ->where('blog_id', $request->blog_id)->paginate(7);


    //     if ($data) {
    //         return response()->json([
    //             'status' => 'success',
    //             'data' => $data,
    //         ]);
    //     } else {
    //         return response()->json([
    //             'Status' => 'Error',
    //             'message' => "Internal server error",
    //         ], 500);
    //     }
    // }
}
