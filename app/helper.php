<?php

use Illuminate\Support\Facades\Route;

function set_none_navbar($uri, $output = ' d-none')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}

function generateCSS($name){
  return "css/$name.css?v=1.0.4";
}

function set_active_navbar($uri, $output = ' active')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}


function cutTitle($title){
  if(strlen($title) > 20){
    $array = explode(" ",$title);
    $count = count($array);
    if ($count <= 5) {
      return $title;
    }else{
      $result = "";
      for ($i=0; $i < 5 ; $i++) { 
        $result = $result." ".$array[$i];
      }
      return $result."...";
    }
  }else{
    return $title;
  }
}