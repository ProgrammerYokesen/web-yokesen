<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('status')->default('lead');
            $table->integer('admin_id')->nullable();
            $table->string('ip_address');
            $table->string('device');
            $table->string('lat_rumah')->nullable();
            $table->string('lng_rumah')->nullable();
            $table->string('screen')->nullable();
            $table->string('platform');
            $table->string('version_platform');
            $table->string('browser');
            $table->string('version_browser');
            $table->string('languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
