@extends("crudbooster::admin_template")
@section("content")
{{-- statistik --}}
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $meetCount }}</h3>

                <p>Jadwal Meeting</p>
            </div>
            <div class="icon">
                <i class="ion ion-clock"></i>
            </div>
            <a href="/admin/booking_meet" class="small-box-footer">More info <i
                    class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-gray">
            <div class="inner">
                <h3>{{ $leadCount }}</h3>

                <p>User Baru</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
            <a href="/admin/leads" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ $contactCount }}</h3>

                <p>Contact</p>
            </div>
            <div class="icon">
                <i class="ion ion-iphone"></i>
            </div>
            <a href="/admin/contacts" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ $winCount }}</h3>

                <p>Win</p>
            </div>
            <div class="icon">
                <i class="ion ion-heart"></i>
            </div>
            <a href="/admin/wins" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{{ $loseCount }}</h3>

                <p>Lose</p>
            </div>
            <div class="icon">
                <i class="ion ion-close-round"></i>
            </div>
            <a href="/admin/loses" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
{{-- end statistik --}}

{{-- table --}}
<div class="row">
    {{-- aktvitas terbaru --}}
    <div class="col-xs-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Activity</h3>

            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Clint</th>
                        <th>Activity</th>
                        <th>Admin</th>
                    </tr>
                    @php
                    $no = 1;
                    @endphp
                    @foreach ($activities as $item)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $item->user_name }}</td>
                        <td>{{ $item->activity }}</td>
                        <td>{{ $item->admin_name }}</td>
                    </tr>
                    @php
                    $no++;
                    @endphp
                    @endforeach

                </table>
                {{ $activities->links() }}
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    {{-- end aktvitas terbaru --}}

    {{-- visitor terbaru --}}
    <div class="col-xs-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Visitor</h3>

            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Domain</th>
                        <th>Landing Page</th>
                    </tr>
                    @php
                    $no = 1;
                    @endphp
                    @foreach ($visitors as $item)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>user</td>
                        <td>{{ $item->landing_page }}</td>
                        <td>{{ $item->landing_domain }}</td>
                    </tr>
                    @php
                    $no++;
                    @endphp
                    @endforeach

                </table>
                {{ $visitors->links() }}
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    {{-- end visitor terbaru --}}
</div>
{{-- endtable --}}
@endsection