<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    @yield('meta_tag')    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="7kmXniFtvRR86TH6PMk1J_8bBKA0V2BKq2PlFvz9zGc" />


    <!-- Title -->
    <title>Yokesen @yield('title_tag')</title>

    <!-- favicon icon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">

    <!-- inject css start -->

    <!--== bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <!--== animate -->
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css" />

    <!--== fontawesome -->
    <link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet" type="text/css" />

    <!--== themify -->
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet" type="text/css" />

    <!--== magnific-popup -->
    <link href="{{ asset('css/magnific-popup/magnific-popup.css') }}" rel="stylesheet" type="text/css" />

    <!--== owl-carousel -->
    <link href="{{ asset('css/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" />

    <!--== spacing -->
    <link href="{{ asset('css/spacing.css') }}" rel="stylesheet" type="text/css" />

    <!--== base -->
    <link href="{{ asset('css/base.css') }}" rel="stylesheet" type="text/css" />

    <!--== shortcodes -->
    <link href="{{ asset('css/shortcodes.css') }}" rel="stylesheet" type="text/css" />

    <!--== default-theme -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />

    <!--== responsive -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/theme-color/theme-color-2.css') }}" rel="stylesheet" type="text/css" />

    <!-- inject css end -->
    @yield('additional_assets')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-199554664-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-199554664-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '226310869518857');
        fbq('track', 'PageView');
    </script>


    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KFT5XT3');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFT5XT3" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- page wrapper start -->

    <div class="page-wrapper">

        @include('components.navbar')
        @yield('content')
        @include('components.footer')
    </div>

    <!-- page wrapper end -->

    <div class="chat__wa"><i class="fab fa-whatsapp"></i></div>

    <!--back-to-top start-->

    <div class="scroll-top"><a class="smoothscroll" href="#top"><i class="flaticon-upload"></i></a></div>

    <!--back-to-top end-->

    <!-- inject js start -->
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=226310869518857&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <!--== theme -->
    <script src="{{ asset('js/theme.js') }}"></script>

    <!--== magnific-popup -->
    <script src="{{ asset('js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

    <!--== owl-carousel -->
    <script src="{{ asset('js/owl-carousel/owl.carousel.min.js') }}"></script>

    <!--== counter -->
    <script src="{{ asset('js/counter/counter.js') }}"></script>

    <!--== countdown -->
    <script src="{{ asset('js/countdown/jquery.countdown.min.js') }}"></script>

    <!--== isotope -->
    <script src="{{ asset('js/isotope/isotope.pkgd.min.js') }}"></script>

    <!--== mouse-parallax -->
    <script src="{{ asset('js/mouse-parallax/tweenmax.min.js') }}"></script>
    <script src="{{ asset('js/mouse-parallax/jquery-parallax.js') }}"></script>

    <!--== wow -->
    <script src="{{ asset('js/wow.min.js') }}"></script>

    <!--== theme-script -->
    <script src="{{ asset('js/theme-script.js') }}"></script>
    
    <!-- inject js end -->

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()

            $('.chat__wa, .btn_wa').on('click', function() {
                var y = Math.random();
                if (y < 0.5) {
                    y = 0
                } else {
                    y = 1
                }
                let wa = [6281388886435, 6281313002582]
                window.open(
                    `https://wa.me/${wa[y]}?text=Halo%2C%20aku%20mau%20bertanya%20tentang%20servis%20di%20Yokesen`
                )
            })
        })
    </script>
    <script>
        function openWindow(route){
            window.location.href = route;
        }
    </script>
    @yield('asset_js')

</body>

</html>
