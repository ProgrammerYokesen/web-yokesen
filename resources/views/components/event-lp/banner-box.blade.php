<div class="container " style="height: 370px">

    <div class="card h-100" style="border: 3px solid rgb(191, 129, 49)">
        <div class="card-body">
            <div class="d-flex align-items-center h-100">
                <div class="row mx-3">
                    <div class="col-md-6 order-md-1 order-2 my-auto">
                        <h3 class="title-banner">
                            
                            {{$title}}
                        </h3>
                        <p class="detail-banner">
                            {{$detail}}
                           </p>
                    </div>
                    <div class="col-md-6 order-md-2 order-1 ">
                        <img src="{{$asset}}" alt=""
                            class="banner-img">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>