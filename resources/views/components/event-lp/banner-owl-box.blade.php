<div class="item">
    <div class="testimonial style-2 w-100">
        <div class="row align-items-center">
            
            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                <div class="testimonial-content">
                    <img src="{{$src}}" alt="" class="img-logo-banner">
                    <div class="testimonial-caption">
                        <h5>{{$title}}</h5>
                    </div>
                    <p class="w-100">
                        {{-- {{ trans('service.service-item3-desc') }} --}}
                        {{$detail}}
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="testimonial-img info-img round-animation">
                    <img class="img-fluid leftRight" src="{{$banner}}" alt="">
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</div>