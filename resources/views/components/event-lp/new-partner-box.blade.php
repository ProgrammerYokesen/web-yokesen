<div class="col-lg-3 col-md-4 col-6 my-auto">
    <div class="row">
        <div class="col-6">
            <div class="div-partner short mb-6 shadow rounded">
                <img src="{{ $url }}" alt="" class="img-partner">
                <div class="position-absolute info">
                    <div class="d-flex justify-content-center align-items-center h-100">
                        <h6 style="color: white" class="text-center">
                            {{ $name }}
                        </h6>
                    </div>

                </div>
            </div>
        </div>
        @if ($url2 != '' && $name2 != '')
            <div class="col-6">
                <div class="div-partner short mb-6 shadow rounded">
                    <img src="{{ $url2 }}" alt="" class="img-partner">
                    <div class="position-absolute info">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <h6 style="color: white" class="text-center">
                                {{ $name2 }}
                            </h6>
                        </div>

                    </div>
                </div>
            </div>
        @endif

    </div>


</div>
