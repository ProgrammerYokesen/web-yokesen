<div class="col-lg-3 col-md-4 col-6 my-auto">
    <div class="div-partner border-1 mb-6 shadow" >
        <img src="{{$url}}" alt="" class="img-partner">
        
        <div class="position-absolute info rounded" style="cursor: pointer" onclick="openWindow('{{$hyperlink}}')">
            <div class="d-flex justify-content-center align-items-center h-100">
                <h5 style="color: white" class="text-center">
                    {{$name}}
                </h5>
            </div>
            
        </div>
    </div>

</div>