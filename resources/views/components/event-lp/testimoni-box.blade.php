<div class="card shadow mb-3 mx-md-0 mx-3">
    <div class="card-body">
        <div class="row">
            <div class="col-md-5">
                <img src="{{$img}}" alt="" class="img-testi mb-md-0 mb-3">
            </div>
            <div class="col-md-7">
                <p class="testi-detail">{{$details}}</p>

                <h6 class="testi-name">{{$name}}</h6>
            </div>
        </div>
    </div>
</div>