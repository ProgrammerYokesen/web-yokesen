 <!--footer start-->

 <footer class="footer white-bg z-index-1 overflow-hidden bg-contain" data-bg-img="images/bg/09.png">
     <div class="round-p-animation"></div>
     <div class="primary-footer {{set_none_navbar('eventLP')}}">
         <div class="container-fluid p-0">
             <div class="row">
                 <div class="col-lg-4">
                     <div class="ht-theme-info bg-contain bg-pos-r h-100 light-bg text-black"
                         data-bg-img="images/bg/right.png">
                         <div class="footer-logo">
                             <a href="https://yokesen.com">
                                 <img class="img-fluid" src="{{ asset('images/logo-color-2.png') }}" alt="">
                             </a>
                         </div>
                         <p class="mb-3">
                             Ruko Crystal 8 no 18, Alam Sutera, Pakualam, <br> Kec. Serpong Utara, Kota Tangerang
                             Selatan, <br> Banten 15320
                         </p>
                         {{-- <a class="btn-simple"
                             href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> --}}
                         <div class="social-icons social-border circle social-hover mt-5">
                             <h4 class="title">Follow Us</h4>
                             <ul class="list-inline">
                                 <li class="social-facebook"><a href="https://www.facebook.com/yokesen.id"
                                         target="_blank"><i class="fab fa-facebook-f"></i></a>
                                 </li>
                                 {{-- <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                                 </li> --}}
                                 <li class="social-gplus"><a href="https://www.instagram.com/yokesen_id/"
                                         target="_blank"><i class="fab fa-instagram"></i></a>
                                 </li>
                                 <li class="social-linkedin"><a href="https://id.linkedin.com/company/yokesentechnology"
                                         target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                 </li>

                             </ul>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-8 py-8 px-5">
                     <div class="row">
                         <div class="col-lg-6 col-md-6 footer-list mb-5">
                             <h6 class="title">Want to collaborate?</h6>
                             <a href="mailto:contact@yokesen.com" class="footer_btn route">contact@yokesen.com</a>
                             <h6 class="title mt-6">Get to Know Us</h6>
                             <div class="mb-3">
                                <a href="{{ route('aboutPage') }}" class="footer_btn route" style="padding: 0.5rem 61px">Company</a>
                             </div>
                             <div class="mb-3">
                                <a href="{{ route('digitalPage') }}" class="footer_btn route" style="padding: 0.5rem 50px">Our Services</a>

                             </div>
                             {{-- <div class="mb-3">
                                <a href="{{ route('portoPage') }}" class="footer_btn route" style="padding: 0.5rem 56px">Our Clients</a>

                             </div> --}}
                             <div class="mb-3">
                                <a href="{{ route('blogPage') }}" class="footer_btn route" style="padding: 0.5rem 85px">Blog</a>

                             </div>
                             <div class="mb-3">
                                <a href="{{ route('sitemapPage') }}" class="footer_btn route" style="padding: 0.5rem 68px">Sitemap</a>

                             </div>
                            
                         </div>
                         <div class="col-lg-6 col-md-6 mt-5 mt-md-0 mb-5">
                             <h6 class="title">Want to join us?</h6>
                             <a href="mailto:human.resources@yokesen.com"
                                 class="footer_btn">human.resources@yokesen.com</a>
                         </div>
                         {{-- <div class="col-lg-6 col-md-6 mt-5">
                             <h6 class="title">Want to join us?</h6>
                             <a href="mailto:human.resources@yokesen.com">human.resources@yokesen.com</a>
                         </div> --}}
                         {{-- <div class="col-lg-6 col-md-6 mt-5">
                             <h6 class="title">Want to learn?</h6>
                             <a href="mailto:contact@yokesen.com">intern@yokesen.com</a>
                         </div> --}}
                     </div>
                     {{-- <div class="row mt-5">
                         <div class="col-lg-10 col-md-12 me-auto">
                             <div
                                 class="align-items-center white-bg box-shadow px-3 py-3 radius d-md-flex justify-content-between">
                                 <h4 class="mb-0">NewsLetter</h4>
                                 <div class="subscribe-form sm-mt-2">
                                     <form id="mc-form" class="group">
                                         <input type="email" value="" name="EMAIL" class="email" id="mc-email"
                                             placeholder="Email Address" required="">
                                         <input class="btn btn-theme" type="submit" name="subscribe" value="Subscribe">
                                     </form>
                                 </div>
                             </div>
                         </div>
                     </div> --}}
                 </div>
             </div>
         </div>
     </div>
     <div class="secondary-footer">
         <div class="container">
             <div class="copyright">
                 <div class="row align-items-center">
                     <div class="col-md-6"> <span>Copyright 2021 | All Rights Reserved</span>
                     </div>
                     <div class="col-md-6 text-md-end sm-mt-2"> <span>Yokesen Teknologi Indonesia</span>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>

 <!--footer end-->
