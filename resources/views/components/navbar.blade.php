<!--header start-->
<header id="site-header" class="header">
    <div id="header-wrap">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand logo" href="{{ route('homePage') }}">
                            <img id="logo-img" class="img-fluid" src="{{ asset('images/logo-color-2.png') }}"
                                alt="empty yokesen" style="width: 150px;height:unset">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation"> <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse " id="navbarNav">
                            <ul class="{{ set_none_navbar('eventLP') }} navbar-nav ms-auto me-3 position-relative">
                                <!-- Home -->
                                {{-- <li class="nav-item">
                                    <a class="nav-link active" href="{{ route('homePage') }}">Home</a>
                                </li> --}}
                                <li class="nav-item ">
                                    <a class="nav-link {{ set_active_navbar('aboutPage') }}"
                                        href="{{ route('aboutPage') }}">Company</a>
                                </li>
                                <li class="nav-item dropdown"> <a
                                        class="nav-link dropdown-toggle {{ set_active_navbar('digitalPage') }} "
                                        href="{{ route('digitalPage') }}" data-bs-toggle="dropdown">Services</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item ">
                                            <a class="nav-link {{ set_active_navbar('digitalPage') }}"
                                                href="{{ route('digitalPage') }}">Our Services</a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link {{ set_active_navbar('thisThatPage') }}"
                                                href="{{ route('thisThatPage') }}">From that to
                                                this</a>
                                        </li>
                                        {{-- <li class="nav-item  ">
                                            <a class="nav-link {{ set_active_navbar('portoPage') }}"
                                                href="{{ route('portoPage') }}">Our Client</a>
                                        </li> --}}
                                        <li class="nav-item">
                                            <a class="nav-link {{ set_active_navbar('gamificationPage') }}"
                                                href="{{ route('gamificationPage') }}">Gamification <img
                                                    src="{{ asset('images/icons/game-console.svg') }}" alt="ico"
                                                    style="width: 20px; margin-left:5px">
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link {{ set_active_navbar('blogPage') }}"
                                        href="{{ route('blogPage') }}">Blog</a>
                                </li>
                                <li
                                    class="nav-item {{ set_active_navbar('webinarPage') }}
                                {{-- nav-game --}}
                                ">
                                    <a class="nav-link" href="{{ route('webinarPage') }}">Webinar</a>
                                </li>

                                {{-- <li class="nav-item">
                                    <a class="nav-link" href="{{ route('digitalPage') }}">Digital Solution</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('thisThatPage') }}">From that to this</a>
                                </li>
                                <li class="nav-item nav-game">
                                    <a class="nav-link " href="javascript:void(0)">Gamification <img
                                            src="{{ asset('images/icons/game-console.svg') }}" alt="ico"
                                            style="width: 20px; margin-left:5px">
                                    </a>
                                </li> --}}


                                {{-- <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">Pages</a>
                                    <div class="dropdown-menu">
                                        <ul class="list-unstyled">
                                            <li><a href="about-us.html">About Us 1</a>
                                            </li>
                                            <li><a href="about-us-2.html">About Us 2</a>
                                            </li>
                                            <li><a href="team.html">Team</a>
                                            </li>
                                            <li><a href="team-single.html">Team Single</a>
                                            </li>
                                            <li><a href="faq.html">Faq</a>
                                            </li>
                                            <li class="dropdown-submenu"> <a href="#" class="dropdown-toggle">
                                                    Case Studies
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="case-studies-grid-2.html">Case Studies Column
                                                            2</a>
                                                    </li>
                                                    <li><a href="case-studies-grid-3.html">Case Studies Column
                                                            3</a>
                                                    </li>
                                                    <li><a href="case-studies-fullwidth.html">Case Studies
                                                            FullWidth</a>
                                                    </li>
                                                    <li><a href="case-studies-single.html">Case Studies
                                                            Single</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"> <a href="#" class="dropdown-toggle">
                                                    Features
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="shortcode-accordions.html">Accordion</a>
                                                    </li>
                                                    <li><a href="shortcode-blog-post.html">Blog Post</a>
                                                    </li>
                                                    <li><a href="shortcode-counter.html">Counter</a>
                                                    </li>
                                                    <li><a href="shortcode-feature-box.html">Featured Box</a>
                                                    </li>
                                                    <li><a href="shortcode-pricing.html">Pricing Table</a>
                                                    </li>
                                                    <li><a href="shortcode-team.html">Team</a>
                                                    </li>
                                                    <li><a href="shortcode-testimonial.html">Testimonials</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="coming-soon.html">Coming Soon</a>
                                            </li>
                                            <li><a href="error-404.html">Error 404</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li> --}}
                                {{-- <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">Services</a>
                                    <div class="dropdown-menu">
                                        <ul class="list-unstyled">
                                            <li><a href="service.html">Service 1</a>
                                            </li>
                                            <li><a href="service-2.html">Service 2</a>
                                            </li>
                                            <li><a href="service-single.html">Service Single</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">Project</a>
                                    <div class="dropdown-menu">
                                        <ul class="list-unstyled">
                                            <li><a href="project-grid-2.html">Project Grid 2</a>
                                            </li>
                                            <li><a href="project-grid-3.html">Project Grid 3</a>
                                            </li>
                                            <li> <a href="project-fullwidth.html">Project FullWidth</a>
                                            </li>
                                            <li> <a href="project-details.html">Project Details</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">News</a>
                                    <div class="dropdown-menu">
                                        <ul class="list-unstyled">
                                            <li><a href="blog-classic.html">Blog Classic</a>
                                            </li>
                                            <li><a href="blog-grid-2.html">Blog Grid 2</a>
                                            </li>
                                            <li><a href="blog-grid-3.html">Blog Grid 3</a>
                                            </li>
                                            <li><a href="blog-left-sidebar.html">Blog left sidebar</a>
                                            </li>
                                            <li><a href="blog-right-sidebar.html">Blog right sidebar</a>
                                            </li>
                                            <li><a href="blog-details.html">Blog Single</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">Contact</a>
                                    <div class="dropdown-menu">
                                        <ul class="list-unstyled">
                                            <li><a href="contact.html">Contact us 1</a>
                                            </li>
                                            <li><a href="contact-2.html">Contact us 2</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li> --}}
                            </ul>
                        </div>
                        <div class="right-nav align-items-center d-flex justify-content-end">
                            <a class="btn btn-white btn-sm" href="{{ route('contactPage') }}">Contact Us</a>
                        </div>
                        <ul>
                            <li class="nav-item dropdown"> <a class="nav-link active dropdown-toggle" href="#"
                                    data-bs-toggle="dropdown">{{ $currentLanguage->locale }}</a>
                                <ul class="dropdown-menu bahasa" style="padding:0">
                                    @foreach ($altLocalizedUrls as $alt)
                                        <li><a href="{{ $alt['url'] }}"
                                                hreflang="{{ $alt['locale'] }}">{{ $alt['locale'] }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!--header end-->
