<div class="col-lg-4 col-md-6">
    <div class="row">
        <div class="col-6 mt-2">
            <div class="sim-div-white">
                <div class="d-flex align-items-center justify-content-center h-100 p-3">
                    <p class="text-center">{{$title1}}</p>
                </div>
            </div>
        </div>
        <div class="col-6 mt-2">
            <img src="{{$img2}}"
                alt="" class="img-section-1">
        </div>
        <div class="col-6 mt-2">
            <img src="{{$img1}}"
                alt="" class="img-section-1">
        </div>
        <div class="col-6 mt-2">
            <div class="sim-div-blue">
                <div class="d-flex align-items-center justify-content-center h-100 p-3">
                    <p class="text-center">{{$title2}}</p>
                </div>
            </div>
        </div>
    </div>

</div>