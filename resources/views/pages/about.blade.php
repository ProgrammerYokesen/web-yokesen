@extends('app')
@section('meta_tag')
    <meta name="description" content="{{ trans('about.about-meta-desc') }}" />
    <meta name="keywords" content="{{ trans('about.about-meta-keyword') }}" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | Company
@endsection
@section('additional_assets')
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">
@endsection
@section('content')
    <!--page title start-->

    {{-- <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('tentang.tentang-title') }}</h1>
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Pages</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">About Us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="page-title-pattern"><img class="img-fluid" src="images/bg/06.png" alt=""></div>
    </section> --}}

    <!--page title end-->
    <div class="head-page">
        {{-- <div class="container"> --}}
        <img src="{{ asset('images/banner/yokesen2.jpg') }}" alt="" class="img-about">
        {{-- </div> --}}
    </div>

    
    <!--body content start-->

    <div class="page-content">

        <!--about start-->

        <section data-bg-img="images/bg/09.png">

            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 col-md-12 mt-5 mt-lg-0">
                        <div class="section-title mb-3">
                            <h6 style="text-transform: uppercase; font-weight: 300;">Yokesen</h6>
                        </div>
                        <h5 class="text-black" style="font-weight: 300;">
                            {{ trans('tentang.tentang-desc') }}
                        </h5>
                    </div>
                </div>
            </div>
        </section>

        <!--about end-->

        <!--case studies start-->
        <section class="overflow-hidden dark-bg custom-pb-18 animatedBackground" data-bg-img="images/pattern/about.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title mb-0">
                            <!-- <h6>SUCCESS STORIES</h6> -->
                            <h2 class="title">{{ trans('tentang.tentang-vision') }}</h2>
                            <p class="mb-0 text-white">
                                {{ trans('tentang.tentang-vision-desc') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--case studies end-->

        <!--client logo start-->
        {{-- <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ht-clients d-flex flex-wrap align-items-center text-center">
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/bayer.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/delicute.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/lpcb.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/wgm.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        <!--client logo end-->

        {{-- CEO Section --}}
        <section class="bg-cover" data-bg-img="images/bg/09.png">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-bg box-shadow">
                            <div class="row align-items-center">
                                <div class="col-lg-5 col-md-12">
                                    <div class="team-single">
                                        <div class="team-images p-4">
                                            <img class="img-fluid w-100" src="{{ asset('images/teams/Yoke.png') }}"
                                                alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 me-auto col-md-12">
                                    <div class="team-description md-px-3 md-py-3">
                                        <h6 class="text-theme">CEO</h6>
                                        <h2 class="title z-index-1 mb-2">Yoke Setiawan Endarto</h2>
                                        <p class="lead">
                                            {{ trans('tentang.tentang-ceo') }}
                                        </p>
                                        <p>
                                            {{ trans('tentang.tentang-ceo2') }}
                                        </p>
                                        <p>
                                            {{ trans('tentang.tentang-ceo3') }}
                                        </p>
                                        {{-- <ul class="team-icon list-unstyled">
                                            <li class="mb-3"> <i class="flaticon-contact"></i>
                                                <a href="mailto:themeht23@gmail.com">themeht23@gmail.com</a>
                                            </li>
                                            <li> <i class="flaticon-telephone-symbol-button"></i>
                                                <a href="tel:+912345678900">+91-234-567-8900</a>
                                            </li>
                                        </ul> --}}
                                        {{-- <div class="social-icons social-hover mt-4">
                                            <ul class="list-inline mb-0">
                                                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"
                                                            aria-hidden="true"></i></a>
                                                </li>
                                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"
                                                            aria-hidden="true"></i></a>
                                                </li>
                                                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"
                                                            aria-hidden="true"></i></a>
                                                </li>
                                                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"
                                                            aria-hidden="true"></i></a>
                                                </li>
                                                <li class="social-rss"><a href="#"><i class="fas fa-rss"
                                                            aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- CEO Section end --}}

        <!--board start-->
        <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Board of Directors</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row box-fade">
                        <section class="overflow-hidden pt-0 custom-mt-10 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="2" data-md-items="1"
                                            data-sm-items="1" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Yoke.png') }}" alt=""
                                                            style="width: 300px; margin: 0 auto">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Yoke Setiawan Endarto</a>
                                                        </h5>
                                                        <span>CEO</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Yuli.png') }}" alt=""
                                                            style="width: 300px; margin: 0 auto">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Yuliana Pannieragouw</a></h5>
                                                        <span>Komisaris</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Anastasia.png') }}" alt=""
                                                            style="width: 300px; margin: 0 auto">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Anastasia Effendi</a></h5>
                                                        <span>Direktur Keuangan</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </section>
        </div>
        <!--board end-->

        <!--tech team start-->
        <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Team Leader</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row box-fade">
                        <section class="overflow-hidden pt-0 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2"
                                            data-sm-items="2" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Yusuf.png') }}" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Muhammad Yusuf</a></h5>
                                                        <span>Marketing & Sales Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images d-flex justify-content-center py-5">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Johan.png') }}"
                                                            style="width:180px" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Johan Susanto</a></h5>
                                                        <span>Operational Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Finley.png') }}" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Finley Khouwira</a></h5>
                                                        <span>Technology Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images d-flex justify-content-center py-5">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Deo.png') }}" style="width:180px"
                                                            alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Deo Zoeghema</a></h5>
                                                        <span>Human Resource Development Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images d-flex justify-content-center py-5">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Shinta.png') }}"
                                                            style="width:180px" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Shinta</a>
                                                        </h5>
                                                        <span>Jr. Operational Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images d-flex justify-content-center py-5">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Amira.png') }}"
                                                            style="width:180px" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Amira Luthfia</a>
                                                        </h5>
                                                        <span>Jr. Marketing Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="item">
                                                    <div class="cases-item">
                                                        <div class="cases-images d-flex justify-content-center ">
                                                            <img class="img-fluid" src="{{ asset('images/team/niken.png') }}" style="width:220px" alt="">
                                                        </div>
                                                        <div class="cases-description">
                                                            <h5><a href="#">Arini Niken</a></h5>
                                                            <span>Corporate Secretary</span>
                                                        </div>
                                                    </div>
                                                </div> -->

                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid"
                                                            src="{{ asset('images/teams/Natasha.png') }}" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Natasha Aline</a></h5>
                                                        <span>Jr. Project Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
        <!--tech team end-->

        <!--Multimedia Team start-->
        {{-- <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Multimedia Team</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <section class="overflow-hidden pt-0 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2"
                                            data-sm-items="2" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/01.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Deo Zoeghema</a></h5>
                                                        <span>Human Resource</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/02.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Finley Khouwira</a></h5>
                                                        <span>Junior Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/03.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Pradipta Eva</a></h5>
                                                        <span>UI/UX Designer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/04.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Andika Yuda</a></h5>
                                                        <span>Kuli Digital</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/05.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Dimas Trivianto</a></h5>
                                                        <span>Web Programmer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/06.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Daniel Agustian</a></h5>
                                                        <span>Mobile Developer</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div> --}}
        <!--Multimedia Team end-->

        <!--Marketplace Team start-->
        {{-- <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Marketplace Team</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <section class="overflow-hidden pt-0 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2"
                                            data-sm-items="2" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/01.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Deo Zoeghema</a></h5>
                                                        <span>Human Resource</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/02.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Finley Khouwira</a></h5>
                                                        <span>Junior Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/03.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Pradipta Eva</a></h5>
                                                        <span>UI/UX Designer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/04.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Andika Yuda</a></h5>
                                                        <span>Kuli Digital</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/05.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Dimas Trivianto</a></h5>
                                                        <span>Web Programmer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/06.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Daniel Agustian</a></h5>
                                                        <span>Mobile Developer</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div> --}}
        <!--Marketplace Team end-->

        <!--Digital Strategy Team start-->
        {{-- <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Digital Strategy Team</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <section class="overflow-hidden pt-0 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2"
                                            data-sm-items="2" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/01.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Deo Zoeghema</a></h5>
                                                        <span>Human Resource</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/02.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Finley Khouwira</a></h5>
                                                        <span>Junior Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/03.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Pradipta Eva</a></h5>
                                                        <span>UI/UX Designer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/04.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Andika Yuda</a></h5>
                                                        <span>Kuli Digital</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/05.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Dimas Trivianto</a></h5>
                                                        <span>Web Programmer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/06.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Daniel Agustian</a></h5>
                                                        <span>Mobile Developer</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div> --}}
        <!--Digital Strategy Team end-->

        <!--Account Executive Team start-->
        {{-- <div class="team__section">
            <section class="bg-cover" data-bg-img="images/bg/09.png">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="section-title">
                                <h2 class="title">Account Executive</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <section class="overflow-hidden pt-0 position-relative z-index-1">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2"
                                            data-sm-items="2" data-center="true" data-dots="false" data-autoplay="true">
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/01.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Deo Zoeghema</a></h5>
                                                        <span>Human Resource</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/02.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Finley Khouwira</a></h5>
                                                        <span>Junior Manager</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/03.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Pradipta Eva</a></h5>
                                                        <span>UI/UX Designer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/04.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Andika Yuda</a></h5>
                                                        <span>Kuli Digital</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/05.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Dimas Trivianto</a></h5>
                                                        <span>Web Programmer</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="cases-item">
                                                    <div class="cases-images">
                                                        <img class="img-fluid" src="images/teams/06.png" alt="">
                                                    </div>
                                                    <div class="cases-description">
                                                        <h5><a href="#">Daniel Agustian</a></h5>
                                                        <span>Mobile Developer</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div> --}}
        <!--Account Executive Team end-->

    </div>

    <!--body content end-->
@endsection
