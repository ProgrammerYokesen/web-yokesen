<div class="col-lg-4 col-md-6 col-sm-6 col-12 blog__card">
    <a href="{{ route('detailBlogPage', $slug) }}">
        <img class="blog__img" src="{{ asset($image) }}" alt="{{ $title }}" data-toggle="tooltip"
            data-placement="top" title="{{ $imgTitle }}">
        <div class="blog__title mt-3">
            {{ $title }}
        </div>
        @if ($body)
            <div class="blog__desc mt-1">
                {{ $body }}
            </div>
        @endif
    </a>
    <div class="blog__desc d-flex mt-2" style="z-index: 9999">
        {{-- <div id="like_icon{{ $blogID }}" onclick="handleLike({{ $blogID }})">
            @if (DB::table('blog_likes')->where('blog_id', $blogID)->where('user_id', Auth::id())->first())
                <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M9 1.40165C13.9929 -3.46447 26.4762 5.0507 9 16C-8.47622 5.05177 4.00711 -3.46447 9 1.40165Z"
                        fill="#FFAA3A" />
                </svg>
            @else
                <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5"
                        d="M17.0761 2.45098C16.6504 2.02472 16.1451 1.68659 15.5889 1.45589C15.0326 1.22519 14.4365 1.10645 13.8344 1.10645C13.2323 1.10645 12.6362 1.22519 12.0799 1.45589C11.5237 1.68659 11.0184 2.02472 10.5927 2.45098L9.7094 3.3352L8.82607 2.45098C7.96633 1.59038 6.80027 1.10689 5.58441 1.10689C4.36855 1.10689 3.20248 1.59038 2.34274 2.45098C1.483 3.31159 1 4.47882 1 5.6959C1 6.91298 1.483 8.08021 2.34274 8.94082L3.22607 9.82504L9.7094 16.3149L16.1927 9.82504L17.0761 8.94082C17.5019 8.51476 17.8397 8.0089 18.0702 7.45212C18.3006 6.89535 18.4193 6.29858 18.4193 5.6959C18.4193 5.09322 18.3006 4.49645 18.0702 3.93968C17.8397 3.3829 17.5019 2.87704 17.0761 2.45098V2.45098Z"
                        stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
            @endif
        </div>
        <span id="total-like-{{ $blogID }}" class="ml-1">{{ $likes }}</span>
        <p class="mx-2"></p>
        <div style="cursor: pointer" id="kt_quick_user_toggle">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.5">
                    <path
                        d="M17.5 12.5C17.5 12.942 17.3244 13.366 17.0118 13.6785C16.6993 13.9911 16.2754 14.1667 15.8333 14.1667H5.83333L2.5 17.5V4.16667C2.5 3.72464 2.67559 3.30072 2.98816 2.98816C3.30072 2.67559 3.72464 2.5 4.16667 2.5H15.8333C16.2754 2.5 16.6993 2.67559 17.0118 2.98816C17.3244 3.30072 17.5 3.72464 17.5 4.16667V12.5Z"
                        stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </g>
            </svg>
        </div>
        <span id="total-comment-{{ $blogID }}" class="ml-1">0</span>
        <p class="mx-2"></p> --}}
        <p>{{ date('d M Y', strtotime($date)) }}</p>
    </div>
</div>
