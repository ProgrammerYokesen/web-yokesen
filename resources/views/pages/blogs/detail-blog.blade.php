@section('additional_assets')
    <link href="{{ asset('css/blog/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('js/gs/good-share.min.css') }}">
@endsection

@section('meta_tag')
    <meta name="keywords" content="{{ $blog->blogMetaKeywords }}" />
    <meta name="description" content="{{ $blog->blogMetaDescription }}" />
    <meta name="author" content="https://yokesen.com" />
@endsection

@section('title_tag')
    | {{ $blog->blogTitle }}
@endsection

@extends('app')

@section('content')

    <div class="page-content">
        <div class="d-flex align-items-center">
        </div>
        <section class="container content__body">
            <div class="blog_detail__title black-normal-48 text-center">
                {{ $blog->blogTitle }}
            </div>

            <div class="blog__head_img">
                <img width="100%" src='{{ asset($blog->blogImage1) }}' alt="{{ $blog->altText }}" data-toggle="tooltip"
                    data-placement="top" title="{{ $blog->titleImage }}">
            </div>

            <div class="blog__content black-normal-16">
                {!! $blog->blogContent !!}

                <div class="blog__author" style="align-items: center">
                    <div id="shareBlock"></div>
                </div>
                <a href="/blogs" class="back__blog" style=""><i class="fas fa-chevron-left"></i> Back</a>
            </div>

            <div class="title_inner_shadow" style="margin: 6rem 0 2rem 0">
                Lihat Artikel Lainnya
            </div>

            <div class="list__blogs row">
                @foreach ($blogs as $item)
                    @component('pages.blogs.components.blog__card')
                        @slot('slug') {{ $item->blogSlug }} @endslot
                        @slot('blogID') {{ $item->id }} @endslot
                        @slot('image') {{ $item->blogImage1 }} @endslot
                        @slot('title') {{ $item->blogTitle }} @endslot
                        @slot('body') {!! $item->blogContent !!} @endslot
                        @slot('likes') {{ $item->blogLike }} @endslot
                        @slot('date') {{ $item->created_at }} @endslot
                        @slot('imgTitle') {{ $item->titleImage }} @endslot
                    @endcomponent
                @endforeach
            </div>
        </section>
    </div>
@endsection

@section('asset_js')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/c-share/jquery.c-share.js') }}"></script>

    <script>
        // https://www.linkedin.com/sharing/share-offsite?mini=true&url=https%3A%2F%2Flink.medium.com%2F7FeG7G8xjkb
        $('#shareBlock').cShare({
            data: {
                linkedin: {
                    fa: 'fab fa-linkedin',
                    name: 'Linkedin',
                    href: (url) => {
                        return `https://www.linkedin.com/sharing/share-offsite?mini=true&url=${url}`
                    },
                    show: true
                },
                whatsapp: {
                    fa: 'fab fa-whatsapp',
                    name: 'Whatsapp',
                    href: (url) => {
                        return `whatsapp://send?text=${url}`
                    },
                    show: true
                }
            },
            description: 'Hey folks!, check this awesome blog! ',
            showButtons: ['line', 'fb', 'twitter', 'linkedin', 'whatsapp']
        });
        $(document).ready(function() {
            $('[data-bs-toggle="tooltip"]').tooltip({
                trigger: 'focus'
            })
        });

        //copy link
        function copyUrl() {
            let url = window.location.href;
            navigator.clipboard.writeText(url);
        }
    </script>
@endsection
