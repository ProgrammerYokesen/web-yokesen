@section('additional_assets')
    <link href="{{ asset('css/blog/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">

@endsection
@section('meta_tag')
    <meta name="keywords"
        content="yokesen, digital consultant, blogs, artikel, blog, digital, solution, digital solution" />
    <meta name="description" content="Blog Yokesen Teknologi Indonesia" />
    <meta name="author" content="https://yokesen.com" />
@endsection

@section('title_tag')
    | Blog
@endsection


@extends('app')

@section('content')

    {{-- <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">BLOGS</h1>
                </div>
            </div>
        </div>
    </section> --}}


    <div class="page-content">
        <section class="container" style="margin-top: 75px">
            
            <div class="row">
                <div class="col-lg-9 col-md-8 order-md-1 order-2">
                    <h4 class="text-center title-part">Categories</h4>
                    <div class="row mb-4 mt-4">
                        <div class="col-lg-3 col-md-4 col-6 mb-lg-0 mb-3">
                            <button class="btn category-card">
                                <p class="category-name">Bussiness</p>
                            </button>
                            
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-lg-0 mb-3">
                            <button class="btn category-card">
                                <p class="category-name">Digital Marketing</p>
                            </button>
                            
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-lg-0 mb-md-3">
                            <button class="btn category-card">
                                <p class="category-name">E-Commerce</p>
                            </button>
                            
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-lg-0 mb-md-3">
                            <button class="btn category-card">
                                <p class="category-name">Other Categories</p>
                            </button>
                            
                        </div>
                    </div>
                    @if ($blogs)
                        <div class="list__blogs row">
                            @foreach ($blogs as $item)
                                @component('pages.blogs.components.blog__card')
                                    @slot('slug') {{ $item->blogSlug }} @endslot
                                    @slot('blogID') {{ $item->id }} @endslot
                                    @slot('image') {{ $item->blogImage1 }} @endslot
                                    @slot('title') {{ $item->blogTitle }} @endslot
                                    @slot('body') {!! $item->blogContent !!} @endslot
                                    @slot('likes') {{ $item->blogLike }} @endslot
                                    @slot('date') {{ $item->created_at }} @endslot
                                    @slot('imgTitle') {{ $item->titleImage }} @endslot
                                @endcomponent
                            @endforeach
                        </div>
                    @else
                        <div class="text-center">
                            <p class="black75-normal-18">
                                <strong>Masih belum ada artikel baru.</strong> <br>
                                Stay tune untuk mendapat info terbaru <br>
                                seputar dunia digital.
                            </p>
                        </div>
                    @endif
                </div>
                <div class="col-lg-3 col-md-4 order-md-2 order-1 mb-md-0 mb-4">
                    <div class="latest card">
                        <div class="card-body">
                            <h4 class="title-card">Latest Articles</h4>
                            @if ($latest)
                                @foreach ($latest as $item)
                                    <a href="{{ route('detailBlogPage', $item->blogSlug) }}">
                                        <p class="title-lates">{{ cutTitle($item->blogTitle)  }}</p>
                                    </a>
                                @endforeach
                            @else
                                <p class="black75-normal-18">
                                    <strong>Masih belum ada artikel baru.</strong> <br>
                                    Stay tune untuk mendapat info terbaru <br>
                                    seputar dunia digital.
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="row">
            <div class="col text-center">
                {{ $blogs->links() }}
            </div>
        </div>
    </div>
@endsection
