@extends('app')
@section('meta_tag')
    <meta name="keywords" content="Yokesen, Contact Yokesen, Contact, contact, yokesen, contact yokesen" />
    <meta name="description" content="Contact Yokesen contact@yokesen.com" />
    <meta name="author" content="https://yokesen.com" />
@endsection 

@section('title_tag')
    | Contact
@endsection

@section('content')
    <!--contact title start-->

    <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">Contact Us</h1>
                </div>
            </div>
        </div>
    </section>

    <!--contact title end-->

    <!--body content start-->

    <div class="page-content">

        <!--address start-->

        <section class="form-info text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="info-inner">
                            <h4 class="title">Want to Collaborate?</h4>
                            <ul class="contact-info list-inline">
                                <li><a href="mailto:contact@yokesen.com"> contact@yokesen.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="info-inner">
                            <h4 class="title">Want to say Hi?</h4>
                            <ul class="contact-info list-inline">
                                <li><a href="mailto:contact@gmail.com"> contact@yokesen.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="info-inner">
                            <h4 class="title">Want to Join Us?</h4>
                            <ul class="contact-info list-inline">
                                <li><a href="mailto:human.resources@yokesen.com"> human.resources@yokesen.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--address end-->


        <!--contact start-->

        <section class="contact-1 pt-0" data-bg-img="images/bg/09.png">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 order-lg-1">
                        <div class="contact-main py-5">
                            <div class="section-title mb-2">
                                <h6>Get In Touch</h6>
                                <h2 class="title">Contact Us</h2>
                            </div>
                            <form id="contact-form" class="row g-4 needs-validation" method="post" action="php/contact.php"
                                novalidate>
                                <div class="messages"></div>
                                <div class="col-md-6">
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Name"
                                        required>
                                    <div class="invalid-feedback">Name is required.</div>
                                </div>
                                <div class="col-md-6">
                                    <input id="form_email" type="email" name="email" class="form-control"
                                        placeholder="Email" required>
                                    <div class="invalid-feedback">Valid email is required.</div>
                                </div>
                                <div class="col-md-12">
                                    <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Phone"
                                        required>
                                    <div class="invalid-feedback">Phone is required</div>
                                </div>
                                <div class="col-md-12">
                                    <input id="form_phone" type="text" name="company" class="form-control" placeholder="Company name"
                                        required>
                                    <div class="invalid-feedback">Company Name is required</div>
                                </div>
                                <div class="col-md-12">
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Message"
                                        rows="3" required></textarea>
                                    <div class="invalid-feedback">Please,leave us a message.</div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-theme btn-radius"><span>Send Message</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="map md-iframe h-100">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15864.764287225265!2d106.6514577!3d-6.238529!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x31829a0d3b295ec4!2sYokesen!5e0!3m2!1sen!2sid!4v1621935145050!5m2!1sen!2sid"
                                allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--contact end-->


    </div>

    <!--body content end-->
@endsection
