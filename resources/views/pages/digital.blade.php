@extends('app')
@section('meta_tag')
    <meta name="description" content="{{ trans('digital.digital-meta-desc') }}" />
    <meta name="keywords" content="{{ trans('digital.digital-meta-keyword') }}" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | Our Services
@endsection

@section('additional_assets')
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">
    <link rel="stylesheet" href="{{ asset(generateCSS('custom/new-lp')) }}">

@endsection

@section('content')
    <!--page title start-->

    {{-- <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('service.service-label') }} </h1>
                </div>
            </div>
        </div>
    </section> --}}

    <!--page title end-->
    <div style="height: 80px"></div>

    <!--service start-->
    <div class="page-content">
        <section class="light-bg overflow-hidden">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title">
                            {{-- <h6>Service</h6> --}}
                            <h5 class="title">
                                {{ trans('digital.digital-desc') }}
                            </h5>
                            <p>
                                {{ trans('digital.digital-desc1') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-6 wow fadeInLeft" data-wow-duration="0.6">
                        <div class="featured-item style-3">
                            <div class="featured-icon">
                                <img class="img-digital" src="{{ asset('images/event-lp/technology.png') }}" alt="">
                            </div>
                            <div class="featured-title">
                                <h5>Technology</h5>
                            </div>
                            <div class="featured-desc">
                                <ul>
                                    <li>Website architecture, design & prototyping</li>
                                    <li>Conversion focused website and landing page, tested for optimum results</li>
                                    <li>User Experience, Front-End Development, User Interface Design</li>
                                    <li>Mobile Application development</li>
                                    <li>Software as a Service (SaaS)</li>
                                    <li>Software as a Business (SaaB)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 mt-5 mt-md-0 wow fadeInUp" data-wow-duration="0.8">
                        <div class="featured-item style-3">
                            <div class="featured-icon">
                                <img class="img-digital" src="{{ asset('images/event-lp/digital-strategy.png') }}"
                                    alt="">
                            </div>
                            <div class="featured-title">
                                <h5>Digital Strategy</h5>
                            </div>
                            <div class="featured-desc">
                                <ul>
                                    <li>Digital Branding, Digital Media and Gamification Design & development</li>
                                    <li>Audience Insight, Targeting & Discovery</li>
                                    <li>Media Planning & Buying</li>
                                    <li>Search Engine Optimization (SEO)</li>
                                    <li>Conversion Rate Optimization</li>
                                    <li>KOL/influencer marketing</li>
                                    <li>Lead Generation and Data collection</li>
                                    <li>Reporting and Dashboards</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 mt-5 wow fadeInRight" data-wow-duration="0.8">
                        <div class="featured-item style-3">
                            <div class="featured-icon">
                                <img class="img-digital" src="{{ asset('images/event-lp/consultant.png') }}" alt="">
                            </div>
                            <div class="featured-title">
                                <h5>Technology Consulting</h5>
                            </div>
                            <div class="featured-desc">
                                <ul>
                                    <li>Data-driven Strategy, from eCommerce to complex software, platforms and mobile apps
                                        essential for business growth</li>
                                    <li>Conversion focused Optimization</li>
                                    <li>Performance management</li>
                                    <li>Project management</li>
                                    <li>Balanced Scorecard</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 mt-5 wow fadeInLeft" data-wow-duration="0.6">
                        <div class="featured-item style-3">
                            <div class="featured-icon">
                                <img class="img-digital" src="{{ asset('images/event-lp/marketplace.png') }}" alt="">
                            </div>
                            <div class="featured-title">
                                <h5>Marketplace Optimization</h5>
                            </div>
                            <div class="featured-desc">
                                <ul>
                                    <li>Full Marketplace Management</li>
                                    <li>Digital store, product listing, and digital shelving</li>
                                    <li>Marketplace creative campaign & promotion</li>
                                    <li>Organic & paid marketplace Optimization</li>
                                    <li>Sales & Conversion Rate Optimization</li>
                                    <li>Fulfilment & logistic distribution</li>
                                    <li>Hyperlocal - Consolidated E-commerce Dashboard (CED)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 mt-5 wow fadeInLeft" data-wow-duration="0.6">
                        <div class="featured-item style-3">
                            <div class="featured-icon">
                                <img class="img-digital" src="{{ asset('images/event-lp/multimedia.png') }}" alt="">
                            </div>
                            <div class="featured-title">
                                <h5>Multimedia & Digital Creative</h5>
                            </div>
                            <div class="featured-desc">
                                <ul>
                                    <li>Full Funnel Digital Media Management</li>
                                    <li>Media strategy & Consumer journey</li>
                                    <li>Digital assets to develop and maintain cohesive brand identity</li>
                                    <li>3D Animation, Motion Video, Product Photoshoot, Copywriting</li>
                                    <li>Virtual events, live streaming and webinars</li>
                                    <li>TVC production</li>
                                    <li>Interactive Website</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container mt-5 wow fadeInUp" data-wow-duration="0.8">
                <div class="section-title text-center">
                    <h3 class="title text-center" style="font-weight: 600">
                        Lini Bisnis Yokesen
                    </h3>
                </div>
                <p class="mb-2 text-center">Yokesen's Technology and Digital Strategy <br>Experts are Result Driven in Tackling Today's Business and Digital Transformation Challenges</p>
                
                <div class="row mt-7 justify-content-center">
                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/logo-color-2.png') }}
                        @endslot
                        @slot('name')
                            PT. Yokesen Teknologi Indonesia
                        @endslot
                        @slot('hyperlink')
                            https://yokesen.com
                        @endslot

                    @endcomponent
                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/event-lp/logo-gudangin.png') }}
                        @endslot
                        @slot('name')
                            PT. Gudang Awan Indonesia
                        @endslot
                        @slot('hyperlink')
                            https://gudangin.id
                        @endslot
                    @endcomponent
                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/event-lp/logo-wgm.png') }}
                        @endslot
                        @slot('name')
                            PT. Warisan Perdagangan Nusantara
                        @endslot
                        @slot('hyperlink')
                            https://warisangajahmada.com
                        @endslot
                    @endcomponent
                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/event-lp/logo-kagumi.png') }}
                        @endslot
                        @slot('name')
                            PT. Yokesen Media Kagumi
                        @endslot
                        @slot('hyperlink')
                            https://mediakagumi.com
                        @endslot
                    @endcomponent
                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/event-lp/logo-warisan.png') }}
                        @endslot
                        @slot('name')
                            PT. Warisan Perdagangan Digital
                        @endslot
                        @slot('hyperlink')
                            https://warisan.co.id
                        @endslot
                    @endcomponent

                    @component('components.event-lp.partner-box')
                        @slot('url')
                            {{ asset('images/event-lp/logo-resonansi.png') }}
                        @endslot
                        @slot('name')
                            Resonansi
                        @endslot
                        @slot('hyperlink')
                            https://resonansi.co.id
                        @endslot
                    @endcomponent
                </div>


            </div>
        </section>
        
    </div>
    <!--service end-->
@endsection
