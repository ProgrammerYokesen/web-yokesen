<!--begin::Entry-->
@extends('app')

@section('additional_assets')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('css/custom/new-lp.css') }}?v=1.0.3.2" type="text/css">
@endsection


@section('content')
    <div class="d-flex flex-column-fluid flex-column position-relative" id="content-data">
        <section
            class="event-sect fullscreen-banner banner banner-2 p-0 overflow-hidden bg-contain bg-pos-r animatedBackground"
            data-bg-img="{{ asset('images/bg/09.png') }}">
            <div class="mouse-parallax" data-bg-img="images/yokesen/bg.png"></div>

            <div class="h-100 bg-contain bg-pos-rb sm-bg-cover">
                <div class="align-center">
                    <div class="container">
                        <div class="section-title text-center">
                            {{-- <h6>Service</h6> --}}
                            <h3 class="title">
                                {{ trans('digital.digital-desc') }}
                            </h3>
                            <p>

                            </p>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="owl-carousel" data-items="1" data-autoplay="true">

                                    @component('components.event-lp.banner-owl-box')
                                        @slot('banner')
                                            {{ asset('images/event-lp/banner/Gudangin.png') }}

                                        @endslot
                                        @slot('src')
                                            {{ asset('images/event-lp/logo-gudangin2.png') }}

                                        @endslot
                                        @slot('title')
                                            gudangin
                                        @endslot
                                        @slot('detail')
                                            Jualan Lebih Banyak. Kirim
                                            Lebih Cepat. Kemanapun Tetap
                                            Lebih Hemat
                                        @endslot
                                    @endcomponent
                                    @component('components.event-lp.banner-owl-box')
                                        @slot('banner')
                                            {{ asset('images/event-lp/banner/WGM.png') }}
                                        @endslot
                                        @slot('src')
                                            {{ asset('images/portofolio/WGM.png') }}

                                        @endslot
                                        @slot('title')
                                            Warisan Gajah Mada
                                        @endslot
                                        @slot('detail')
                                            Warisan Gajah Mada merupakan online shop yang telah berpengalaman selama lebih dari 3
                                            tahun dalam
                                            menjual produk minuman kesehatan maupun produk-produk lainnya dengan konsep digital
                                            marketing yang
                                            efektif & dapat diandalkan.
                                        @endslot
                                    @endcomponent
                                    @component('components.event-lp.banner-owl-box')
                                        @slot('banner')
                                            {{ asset('images/event-lp/banner/Kagumi.png') }}
                                        @endslot
                                        @slot('src')
                                            {{ asset('images/event-lp/logo-kagumi.png') }}

                                        @endslot
                                        @slot('title')
                                            Kagumi
                                        @endslot
                                        @slot('detail')
                                            Kagumi hadir untuk membantumu membangun komunitas
                                            untuk brand melalui Digital Activation dan mengubahnya
                                            menjadi Aset yang Berharga
                                        @endslot
                                    @endcomponent
                                    @component('components.event-lp.banner-owl-box')
                                        @slot('banner')
                                            {{ asset('images/event-lp/banner/Warisan.png') }}

                                        @endslot
                                        @slot('src')
                                            {{ asset('images/event-lp/logo-warisan.png') }}

                                        @endslot
                                        @slot('title')
                                            Warisan
                                        @endslot
                                        @slot('detail')
                                            Menjadi sosok hebat bisa dimulai dari hal-hal kecil. WARISAN hadir menjadi wadah untuk
                                            seluruh
                                            Perempuan yang siap menjadi Panutan. Kamukah orangnya?
                                        @endslot
                                    @endcomponent
                                    @component('components.event-lp.banner-owl-box')
                                        @slot('banner')
                                            {{ asset('images/event-lp/banner/Teratur.png') }}

                                        @endslot
                                        @slot('src')
                                            {{ asset('images/event-lp/logo-teratur.png') }}

                                        @endslot
                                        @slot('title')
                                            Teratur
                                        @endslot
                                        @slot('detail')

                                            Menghabiskan waktu berjam-jam untuk packing, cetak resi dan kontak kurir?
                                            <br>Terkendala dalam mengatur stok barang dan pencatatan?
                                            <br> <strong>Dengan TERATUR, semua orderan beres dalam waktu 5 menit!
                                            </strong>
                                        @endslot
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div class="d-md-flex d-none justify-content-around mt-4">
            <a href="#section-form-1" class="btn btn__prim">
                Gabung Sekarang
            </a>
            <button class="btn btn_wa mx-md-1 mx-auto">
                <div class="d-flex align-items-center">
                    <i class="fab fa-whatsapp" style="color: white; font-size:16px; margin-right: 8px"></i>
                    Daftar Lewat WA
                </div>

            </button>
        </div>
        <div class="d-md-none d-block justify-content-around mt-4 ">
            <div class=" d-flex justify-content-center">
                <a href="#section-form-1" class="btn btn__prim">
                    Gabung Sekarang
                </a>
            </div>
            <div class=" d-flex justify-content-center mt-3">
                <a href="" class="btn btn_wa mx-md-1 mx-auto">
                    <div class="d-flex align-items-center">
                        <i class="fab fa-whatsapp" style="color: white; font-size:16px; margin-right: 8px"></i>
                        Daftar Lewat WA
                    </div>

                </a>
            </div>

        </div>
    </div>

    {{-- <section class="banner-hero mt-8">

        <div class="inside-padding"></div>

        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">

                    @component('components.event-lp.banner-box')
                    @slot('title')
                    gudangin
                    @endslot
                    @slot('detail')
                    Jualan Lebih Banyak. Kirim
                    Lebih Cepat. Kemanapun Tetap
                    Lebih Hemat
                    @endslot
                    @slot('asset')
                    https://dashboard.gudangin.id/images/gudangin.png
                    @endslot
                    @endcomponent
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </section> --}}
    <!--begin::Container-->

    <section class="section-1 wow fadeInRight" data-wow-duration="1.0">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="title text-center" style="font-weight: 600">
                    Mengapa Yokesen?
                </h2>
            </div>
            <div class="card why-card">
                <div class="card-body">
                    <div class="inside-padding"></div>
                    <h5 class="detail-part text-center">
                        Karena kami ada untuk membantu Anda agar pelanggan Anda memahami
                        lebih baik tentang merek atau produk Anda, membantu Anda mengatasi
                        hambatan teknologi dan memanfaatkan peluang.
                    </h5>
                    <div class="inside-padding"></div>
                </div>
            </div>


        </div>
    </section>

    <section class="section-2">
        <div class="inside-padding"></div>
        <div class="section-title text-center wow fadeInUp" data-wow-duration="0.9">
            <h2 class="title text-center" style="font-weight: 600">
                Layanan Apa Saja yang Disediakan Yokesen?
            </h2>
        </div>

        <div class="container wow fadeInBottom" data-wow-duration="1.0">
            <div class="row justify-content-center mt-10">
                @component('components.event-lp.layanan-box')
                    @slot('asset')
                        {{ asset('images/event-lp/technology.png') }}
                    @endslot
                    @slot('title')
                        Technology
                    @endslot
                    @slot('detail')
                        <li>
                            Website architecture, design & prototyping
                        </li>
                        <li>
                            Conversion focused website and landing page, tested for optimum results

                        </li>
                        <li>
                            User Experience, Front-End Development, User Interface Design

                        </li>
                        <li>
                            Mobile Application development
                        </li>
                        <li>
                            Software as a Service (SaaS)
                        </li>
                        <li>Software as a Business (SaaB)</li>
                    @endslot
                @endcomponent

                @component('components.event-lp.layanan-box')
                    @slot('asset')
                        {{ asset('images/event-lp/digital-strategy.png') }}
                    @endslot
                    @slot('title')
                        Digital Strategy
                    @endslot
                    @slot('detail')
                        <li>
                            Digital Branding, Digital Media and Gamification Design & development
                        </li>
                        <li>
                            Audience Insight, Targeting & Discovery

                        </li>
                        <li>
                            Media Planning & Buying

                        </li>
                        <li>
                            Search Engine Optimization (SEO)

                        </li>
                        <li>
                            Conversion Rate Optimization
                        </li>
                        <li>KOL/influencer marketing</li>
                        <li>Lead Generation and Data collection</li>
                        <li>Reporting and Dashboards</li>
                    @endslot
                @endcomponent

                @component('components.event-lp.layanan-box')
                    @slot('asset')
                        {{ asset('images/event-lp/consultant.png') }}
                    @endslot
                    @slot('title')
                        Technology Consulting
                    @endslot
                    @slot('detail')
                        <li>
                            Data-driven Strategy, from eCommerce to complex software, platforms and mobile apps essential for
                            business growth
                        </li>
                        <li>
                            Conversion focused Optimization
                        </li>
                        <li>
                            Performance management

                        </li>
                        <li>
                            Project management

                        </li>
                        <li>
                            Balanced Scorecard

                        </li>

                    @endslot
                @endcomponent
                @component('components.event-lp.layanan-box')
                    @slot('asset')
                        {{ asset('images/event-lp/marketplace.png') }}
                    @endslot
                    @slot('title')
                        Marketplace Optimization
                    @endslot
                    @slot('detail')
                        <li>
                            Full Marketplace Management

                        </li>
                        <li>
                            Digital store, product listing, and digital shelving

                        </li>
                        <li>
                            Marketplace creative campaign & promotion

                        </li>
                        <li>
                            Organic & paid marketplace Optimization

                        </li>
                        <li>
                            Sales & Conversion Rate Optimization
                        </li>
                        <li>Fulfilment & logistic distribution
                        </li>
                        <li>Hyperlocal - Consolidated E-commerce Dashboard (CED)</li>
                    @endslot
                @endcomponent
                @component('components.event-lp.layanan-box')
                    @slot('asset')
                        {{ asset('images/event-lp/multimedia.png') }}
                    @endslot
                    @slot('title')
                        Multimedia and Digital Creative
                    @endslot
                    @slot('detail')
                        <li>
                            Full Funnel Digital Media Management

                        </li>
                        <li>
                            Media strategy & Consumer journey

                        </li>
                        <li>
                            Media strategy & Consumer journey

                        </li>
                        <li>
                            3D Animation, Motion Video, Product Photoshoot, Copywriting

                        </li>
                        <li>
                            Virtual events, live streaming and webinars

                        </li>
                        <li>TVC production
                        </li>
                        <li>Interactive Website</li>
                    @endslot
                @endcomponent
            </div>
        </div>
        {{-- <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div> --}}
        </div>
    </section>
    <!--begin::Container-->

    </section>
    <section class="section-perusahaan wow fadeInLeft" data-wow-duration="0.9">


        <div class="container">
            <div class="section-title text-center">
                <h2 class="title text-center" style="font-weight: 600">
                    Perusahaan Apa Saja Yang Berada Di Bawah Naungan Yokesen?
                </h2>
            </div>
            <p class="mb-2 text-center">Yokesen's Technology and Digital Strategy <br>Experts are Result Driven in Tackling
                Today's Business and Digital Transformation Challenges</p>

            <div class="row mt-7 justify-content-center">
                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/logo-color-2.png') }}
                    @endslot
                    @slot('name')
                        PT. Yokesen Teknologi Indonesia
                    @endslot
                    @slot('hyperlink')
                        https://yokesen.com
                    @endslot

                @endcomponent
                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/event-lp/logo-gudangin.png') }}
                    @endslot
                    @slot('name')
                        PT. Gudang Awan Indonesia
                    @endslot
                    @slot('hyperlink')
                        https://gudangin.id
                    @endslot
                @endcomponent
                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/event-lp/logo-wgm.png') }}
                    @endslot
                    @slot('name')
                        PT. Warisan Perdagangan Nusantara
                    @endslot
                    @slot('hyperlink')
                        https://warisangajahmada.com
                    @endslot
                @endcomponent
                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/event-lp/logo-kagumi.png') }}
                    @endslot
                    @slot('name')
                        PT. Yokesen Media Kagumi
                    @endslot
                    @slot('hyperlink')
                        https://mediakagumi.com
                    @endslot
                @endcomponent
                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/event-lp/logo-warisan.png') }}
                    @endslot
                    @slot('name')
                        PT. Warisan Perdagangan Digital
                    @endslot
                    @slot('hyperlink')
                        https://warisan.co.id
                    @endslot
                @endcomponent

                @component('components.event-lp.partner-box')
                    @slot('url')
                        {{ asset('images/event-lp/logo-teratur.png') }}
                    @endslot
                    @slot('name')
                        PT. Perdagangan Maya Nusantara
                    @endslot
                    @slot('hyperlink')
                        {{ route('teraturLandingPage') }}
                    @endslot
                @endcomponent
            </div>


        </div>

    </section>
    <section class="section-client wow fadeInRight" data-wow-duration="1.0">
        <div class="section-title text-center ">
            <h2 class="title text-center" style="font-weight: 600">
                Brand Apa Saja Yang Telah Bekerja Sama Dengan Yokesen?
            </h2>
        </div>

        <div class="container">
            <div class="row mt-7 justify-content-center">
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Acaraki.png') }}
                    @endslot
                    @slot('name')
                        Acaraki
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/ACLUB.png') }}
                    @endslot
                    @slot('name2')
                        A-Club
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Bagus.png') }}
                    @endslot
                    @slot('name')
                        Bagus
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Bardi.png') }}
                    @endslot
                    @slot('name2')
                        Bardi
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Bayer.png') }}
                    @endslot
                    @slot('name')
                        Bayer
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/BBR.jpg') }}
                    @endslot
                    @slot('name2')
                        BBR
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Delicute.png') }}
                    @endslot
                    @slot('name')
                        Delicute
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Dermaviduals.jpg') }}
                    @endslot
                    @slot('name2')
                        Dermaviduals
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/DJP.jpg') }}
                    @endslot
                    @slot('name')
                        DJP
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Etoro.png') }}
                    @endslot
                    @slot('name2')
                        Etoro
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Financia.png') }}
                    @endslot
                    @slot('name')
                        Financia
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/iHoney.png') }}
                    @endslot
                    @slot('name2')
                        iHoney
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/IOMI.png') }}
                    @endslot
                    @slot('name')
                        IOMI
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/jakpro.png') }}
                    @endslot
                    @slot('name2')
                        JakPro
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/KopGron.png') }}
                    @endslot
                    @slot('name')
                        Kopi Grontol
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Lasegar.png') }}
                    @endslot
                    @slot('name2')
                        Lasegar
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/LPCB.png') }}
                    @endslot
                    @slot('name')
                        Larutan Penyegar Cap Badak
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Maxco.png') }}
                    @endslot
                    @slot('name2')
                        Maxco
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Melandas.jpg') }}
                    @endslot
                    @slot('name')
                        Melandas
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/OBIDA.png') }}
                    @endslot
                    @slot('name2')
                        Obida
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/orbi.png') }}
                    @endslot
                    @slot('name')
                        Orbitrade
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Pelindo.jpg') }}
                    @endslot
                    @slot('name2')
                        Pelindo
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/Siangpure.png') }}
                    @endslot
                    @slot('name')
                        Siangpure
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Sweet Monsta.png') }}
                    @endslot
                    @slot('name2')
                        Sweet Monsta
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/taco.png') }}
                    @endslot
                    @slot('name')
                        TACO
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/Telkom.png') }}
                    @endslot
                    @slot('name2')
                        TWG
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/WGM.png') }}
                    @endslot
                    @slot('name')
                        Warisan Gajah Mada
                    @endslot
                    @slot('url2')
                        {{ asset('images/portofolio/WHW.png') }}
                    @endslot
                    @slot('name2')
                        Warisan Hayam Wuruk
                    @endslot
                @endcomponent
                @component('components.event-lp.new-partner-box')
                    @slot('url')
                        {{ asset('images/portofolio/WS.png') }}
                    @endslot
                    @slot('name')
                        Warisan Sriwijaya
                    @endslot
                    @slot('url2')
                        https://upload.wikimedia.org/wikipedia/en/thumb/0/04/TWG_Tea_logo.jpg/220px-TWG_Tea_logo.jpg
                    @endslot
                    @slot('name2')
                        TWG
                    @endslot
                @endcomponent
            </div>
        </div>

    </section>

    <section class="section-4">
        {{-- Testimoni --}}
        <div class="section-title text-center">
            <h2 class="title text-center" style="font-weight: 600">
                Testimoni Customer
            </h2>
        </div>
        <div class="swiper swiperTesti wow fadeInDown" data-wow-duration="0.95">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    @component('components.event-lp.testimoni-box')
                        @slot('img')
                            https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                        @endslot
                        @slot('details')
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged
                        @endslot
                        @slot('name')
                            John Doe
                        @endslot
                    @endcomponent
                </div>
                <div class="swiper-slide">
                    @component('components.event-lp.testimoni-box')
                        @slot('img')
                            https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                        @endslot
                        @slot('details')
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged
                        @endslot
                        @slot('name')
                            John Doe
                        @endslot
                    @endcomponent
                </div>
                <div class="swiper-slide">
                    @component('components.event-lp.testimoni-box')
                        @slot('img')
                            https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                        @endslot
                        @slot('details')
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged
                        @endslot
                        @slot('name')
                            John Doe
                        @endslot
                    @endcomponent
                </div>
                <div class="swiper-slide">
                    @component('components.event-lp.testimoni-box')
                        @slot('img')
                            https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                        @endslot
                        @slot('details')
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged
                        @endslot
                        @slot('name')
                            John Doe
                        @endslot
                    @endcomponent
                </div>
                <div class="swiper-slide">
                    @component('components.event-lp.testimoni-box')
                        @slot('img')
                            https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                        @endslot
                        @slot('details')
                            scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining
                            essentially unchanged
                        @endslot
                        @slot('name')
                            John Doe
                        @endslot
                    @endcomponent
                </div>
            </div>
        </div>
    </section>

    <section class="section-form-1" id="section-form-1">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="title text-center" style="font-weight: 600">
                    Berminat Untuk Diskusi Dengan Account Executive Kami?
                </h2>
            </div>

            <div class="row mt-8">
                <div class="col-md-6">
                    <!-- <img class="img-calendar" src="{{ asset('images/event-lp/calendar.png') }}" alt=""> -->
                    <iframe src="https://calendar.google.com/calendar/embed?src=uon0rocne2ndmlcp14vl2jl5v0%40group.calendar.google.com&ctz=Asia%2FJakarta" style="border: 0"  frameborder="0" scrolling="no" class="img-calendar shadow"></iframe>

                </div>
                <div class="col-md-6">
                    <div class="card h-100">
                        <div class="card-body">
                            <form action="{{ route('meeting-create') }}" class="form-now" id="form-meet"
                                method="POST">
                                @csrf
                                <h5 class=" mb-6 text-center form-title" sty>Meet with Our Account Executive</h5>
                                <input type="text" class="form-control" name="name" placeholder="Name*" required>
                                <div class="error-text" id="error_name"></div>
                                <input type="text" class="form-control" name="email" placeholder="Email*" required>
                                <div class="error-text" id="error_email"></div>
                                <input type="text" class="form-control" name="whatsapp" placeholder="Mobile Number*"
                                    required>
                                <div class="error-text" id="error_whatsapp"></div>
                                <input type="text" class="form-control" name="company" placeholder="Company Name*"
                                    required>
                                <div class="error-text" id="error_company"></div>
                                <input type="date" class="form-control" name="date" placeholder="Tanggal (dd/mm/yy)"
                                    required>
                                <div class="error-text" id="error_date"></div>
                                <input type="time" class="form-control" name="time" placeholder="Jam " required>
                                <div class="error-text" id="error_time"></div>
                                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LdLM1UeAAAAANIPJEOY3Bo_7bysZ5pSdgMMkyc5"></div>

                                <div class="d-flex justify-content-center align-items-center mt-9">
                                    <button class="btn btn__prim btn_meet" disabled>
                                        Book Now
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="section-form-2" id="">
        <div class="container">
            <h3 class="title-part text-center">
                Tertarik Untuk Tumbuh & Terus Berkembang Bersama Yokesen?
            </h3>
            <div class="row mt-8">
                <div class="col-md-6">
                    <div class="card h-100">
                        <div class="card-body">
                            <form action="{{ route('contacUs-create') }}" method="POST" class="form-now"
                                id="form-contact-us">
                                @csrf
                                <h4 class=" mb-6 text-center form-title" sty>Contact Us</h4>
                                <input type="text" class="form-control" name="contact_name" placeholder="Name*" required>
                                <div class="error-text" id="error_contact_name"></div>
                                <input type="text" class="form-control" name="contact_email" placeholder="Email*"
                                    required>
                                <div class="error-text" id="error_contact_email"></div>
                                <input type="text" class="form-control" name="contact_whatsapp"
                                    placeholder="Mobile Number*" required>
                                <div class="error-text" id="error_contact_whatsapp"></div>
                                <input type="text" class="form-control" name="contact_company" placeholder="Company Name*"
                                    required>
                                <div class="error-text" id="error_contact_company"></div>
                                <textarea name="contact_message" rows="4" cols="50" placeholder="Message*" required
                                    class="form-control"></textarea>
                                <div class="error-text" id="error_contact_message"></div>

                                <div class="d-flex justify-content-center align-items-center mt-9">
                                    <button class="btn btn__prim ">
                                        Book Now
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-auto">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.191023410788!2d106.64924071418785!3d-6.238535395484163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f9ceda7c6ab5%3A0xce2a00aeec086f67!2sYOKESEN!5e0!3m2!1sen!2sid!4v1642496107751!5m2!1sen!2sid"
                        class="map-gugel" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <div class="inside-padding"></div>
                    <h4 class="title-part text-center mb-5">Follow Social Media Kami</h4>
                    <div class="d-flex justify-content-center align-items-center">
                        <a href="" class="icon-data">
                            <img src="https://seeklogo.com/images/W/whatsapp-icon-logo-BDC0A8063B-seeklogo.com.png" alt="">
                        </a>
                        <a href="" class="icon-data">
                            <img src="https://seeklogo.com/images/F/facebook-logo-966BBFBC34-seeklogo.com.png" alt="">
                        </a>
                        <a href="" class="icon-data">
                            <img src="https://seeklogo.com/images/Y/youtube-icon-logo-521820CDD7-seeklogo.com.png" alt="">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>






    <!--end::Container-->

    </div>
@endsection


@section('asset_js')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.10/dist/sweetalert2.all.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            cssMode: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
            },
            mousewheel: true,
            keyboard: true,
        });
    </script>
    <script>
        // Valdiation
        $(function() {
            $('#form-meet').on('submit', function(e) {
                let errorName = false;
                let errorEmail = false;
                let errorWhatsapp = false;
                let errorCompany = false;

                let dataName = $('input[name="name"]').val();
                let dataEmail = $('input[name="email"]').val();
                let dataWhatsapp = $('input[name="whatsapp"]').val();
                let dataCompany = $('input[name="company"]').val();


                if (dataName.length < 3) {
                    errorName = true;
                    $("#error_name").text('Nama Tidak Boleh Kosong');
                } else {
                    errorName = false;
                    $("#error_name").text('');
                }
                if (dataEmail.length < 8) {
                    errorEmail = true;
                    $("#error_email").text('Email Tidak Boleh Kosong');
                } else {
                    errorEmail = false;
                    $("#error_email").text('');
                }
                if (dataWhatsapp.length < 9) {
                    errorWhatsapp = true;
                    $("#error_whatsapp").text('Nomor Handphone Tidak Boleh Kosong');
                } else {
                    errorWhatsapp = false;
                    $("#error_whatsapp").text('');
                }
                if (dataCompany.length < 9) {
                    errorCompany = true;
                    $("#error_company").text('Nama Perusahaan Tidak Boleh Kosong');
                } else {
                    errorCompany = false;
                    $("#error_company").text('');
                }

                if (errorCompany || errorEmail || errorName || errorWhatsapp) {
                    e.preventDefault();
                }
            })
            $('#form-contact-us').on('submit', function(e) {
                let errorName = false;
                let errorEmail = false;
                let errorWhatsapp = false;
                let errorCompany = false;
                let errorMessage = false;

                let dataName = $('input[name="contact_name"]').val();
                let dataEmail = $('input[name="contact_email"]').val();
                let dataWhatsapp = $('input[name="contact_whatsapp"]').val();
                let dataCompany = $('input[name="contact_company"]').val();
                let dataCompany = $('input[name="contact_message"]').val();

                if (dataName.length < 3) {
                    errorName = true;
                    $("#error_contact_name").text('Nama Tidak Boleh Kosong');
                } else {
                    errorName = false;
                    $("#error_contact_name").text('');
                }
                if (dataEmail.length < 8) {
                    errorEmail = true;
                    $("#error_contact_email").text('Email Tidak Boleh Kosong');
                } else {
                    errorEmail = false;
                    $("#error_contact_email").text('');
                }
                if (dataWhatsapp.length < 9) {
                    errorWhatsapp = true;
                    $("#error_contact_whatsapp").text('Nomor Handphone Tidak Boleh Kosong');
                } else {
                    errorWhatsapp = false;
                    $("#error_contact_whatsapp").text('');
                }
                if (dataCompany.length < 9) {
                    errorCompany = true;
                    $("#error_contact_company").text('Nama Perusahaan Tidak Boleh Kosong');
                } else {
                    errorCompany = false;
                    $("#error_contact_company").text('');
                }
                console.log(errorCompany + " " + errorWhatsapp + " " + errorE);
                if (errorCompany || errorEmail || errorName || errorWhatsapp) {
                    e.preventDefault();
                }
            })
        })
    </script>
    <script>
        var swiper = new Swiper('.swiperTesti', {
            // Default parameters
            slidesPerView: 1,
            spaceBetween: 15,
            loop: true,
            autoplay: {
                delay: 1500,
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                // 450: {
                //     slidesPerView: 2,
                //     spaceBetween: 20
                // },

                // when window width is >= 640px
                769: {
                    slidesPerView: 2,
                    spaceBetween: 25
                }
            }
        })
    </script>
    <script type="text/javascript">
        function recaptchaCallback(){
            $('.btn_meet').removeAttr('disabled');
        }
    </script>
    @if(Session::has('messageSuccess'))
    <script type="text/javascript">
        Swal.fire(
          'Berhasil Mengatur Meeting',
          'Meeting telah terdaftar.',
          'success'
          )
    </script>
    @endif
    @if(Session::has('errorCapcha'))
      <script type="text/javascript">
          Swal.fire(
            'Anda Belum Memasukan Capcha!',
            'Silahkan Ulang Lagi.',
            'error'
            )
      </script>
    @endif
@endsection


<!--end::Entry-->
