@extends('app')
@section('meta_tag')
    {{-- <meta name="description"
        content="{{ trans('digital.digital-meta-desc') }}" /> --}}
    <meta name="description" content="Klien dari Yokesen" />
    <meta name="keywords" content="klien, portofolio" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | Our Client
@endsection

@section('additional_assets')
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">

@endsection

@section('content')
    <!--page title start-->

    <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">Gamification</h1>
                </div>
            </div>
        </div>
    </section>

    <!--page title end-->


    <!--service start-->
    <div class="page-content">
        <section class="light-bg overflow-hidden">
            <div class="container">
                <div class="section-title mb-3">
                    <h6 style="text-transform: uppercase; font-weight: 300;">Yokesen</h6>
                </div>
                <h5 class="text-black" style="font-weight: 300;">
                    <strong>Gamifikasi</strong> merupakan sebuah konsep yang menggabungkan antara permainan, estetika,
                    dan kemampuan berpikir untuk menarik perhatian user, memotivasi user, serta
                    mempromosikan sebuah brand kepada user, sehingga nantinya akan meningkatkan
                    engagement dan penjualan produk dari website tersebut.

                </h5>
                <div class="margin-part">

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{asset('images/game/game1.png')}}" alt="" class="img-game rounded">
                    </div>
                    <div class="col-md-6">
                        <img src="{{asset('images/game/game2.png')}}" alt="" class="img-game rounded">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--service end-->
@endsection
