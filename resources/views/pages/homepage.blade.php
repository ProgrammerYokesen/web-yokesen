@extends('app')
@section('meta_tag') 
    <meta name="description" content="{{ trans('hero.hero-meta-desc') }}" />
    <meta name="keywords" content="{{ trans('hero.hero-meta-keyword') }}" /> 
    <meta name="robots" content="index, follow" /> 
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection
@section('additional_assets')
<link rel="stylesheet" href="{{ asset('css/custom/new-lp.css') }}?v=1.0.3.3" type="text/css">
@endsection
@section('title_tag')
{{ trans('hero.hero-meta-title') }}
@endsection

@section('content')
    <!--hero section start-->

    <section class="fullscreen-banner banner banner-2 p-0 overflow-hidden bg-contain bg-pos-r animatedBackground"
        data-bg-img="{{ asset('images/bg/09.png') }}">
        <div class="mouse-parallax" data-bg-img="images/yokesen/bg.png"></div>
        <div class="h-100 bg-contain bg-pos-rb sm-bg-cover">
            <div class="align-center">
                <div class="container">
                    {{-- <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="glass-bg box-shadow px-5 py-5 sm-px-3 sm-py-3 xs-px-2 xs-py-2 bg-contain bg-pos-l "
                                data-bg-img="images/bg/01.png">
                                <div class="row align-items-center">
                                    <div class="col-lg-6 col-md-12 text-center">
                                        <img class="img-fluid animated zoomIn delay-2 duration-2"
                                            src="images/yokesen/bg-jumbotron.png" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                                        <h1 class="mb-4 animated fadeInUp duration-2">{{ trans('hero.hero-title') }}
                                            <span class="font-w-5">Yokesen</span>
                                        </h1>
                                        <p class="animated fadeInUp delay-1 duration-2">
                                            {{ trans('hero.hero-desc') }}
                                        </p>
                                        <div class="animated fadeInUp delay-2 duration-2">
                                            <a class="btn btn-theme" href="{{ route('digitalPage') }}">
                                                <span>{{ trans('hero.hero-btn') }}</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="owl-carousel" data-items="1" data-autoplay="true">
                                <div class="item">
                                    <div class="testimonial style-2">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="testimonial-img info-img round-animation">
                                                    <img class="img-fluid leftRight" src="images/services/01.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-caption">
                                                        <h5>{{ trans('service.service-item1') }}</h5>
                                                    </div>
                                                    <p>
                                                        {{ trans('service.service-item1-desc') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial style-2">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="testimonial-img info-img round-animation">
                                                    <img class="img-fluid leftRight" src="images/services/02.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-caption">
                                                        <h5>{{ trans('service.service-item2') }}</h5>
                                                    </div>
                                                    <p>
                                                        {{ trans('service.service-item2-desc') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial style-2">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="testimonial-img info-img round-animation">
                                                    <img class="img-fluid leftRight" src="images/services/03.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-caption">
                                                        <h5>{{ trans('service.service-item3') }}</h5>
                                                    </div>
                                                    <p>
                                                        {{ trans('service.service-item3-desc') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial style-2">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="testimonial-img info-img round-animation">
                                                    <img class="img-fluid leftRight" src="images/services/04.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-caption">
                                                        <h5>{{ trans('service.service-item4') }}</h5>
                                                    </div>
                                                    <p>
                                                        {{ trans('service.service-item4-desc') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="testimonial style-2">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="testimonial-img info-img round-animation">
                                                    <img class="img-fluid leftRight" src="images/services/05.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-caption">
                                                        <h5>{{ trans('service.service-item5') }}</h5>
                                                    </div>
                                                    <p>
                                                        {{ trans('service.service-item5-desc') }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--hero section end-->

    <!--body content start-->

    <div class="page-content">

        <!--about start-->
        <section class="position-relative">
            {{-- <div class="pattern-3">
                <img class="img-fluid rotateme" src="images/pattern/03.png" alt="">
            </div> --}}
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-12">
                        <div class="section-title">
                            <h6 class="abotus">{{ trans('about.about-label') }}</h6>
                            <h2 class="title">{{ trans('about.about-title') }}</h2>
                        </div>
                        <p>
                            {{ trans('about.about-desc') }}
                        </p>
                    </div>
                    <div class="col-lg-7 col-md-12 mt-5 mt-lg-0">
                        <div class="owl-carousel no-pb" data-dots="false" data-items="2" data-sm-items="1"
                            data-autoplay="true">
                            <div class="item">
                                <div class="featured-item text-center style-2 mx-3 my-3" style="min-height: 605px">
                                    <div class="featured-icon">
                                        <img class="img-fluid" src="images/feature/03.png" alt="">
                                    </div>
                                    <div class="featured-title">
                                        <h5>{{ trans('about.about-item1') }}</h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p>
                                            {{ trans('about.about-item1-desc') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="featured-item text-center style-2 mx-3 my-3" style="min-height: 605px">
                                    <div class="featured-icon">
                                        <img class="img-fluid" src="images/feature/02.png" alt="">
                                    </div>
                                    <div class="featured-title">
                                        <h5>{{ trans('about.about-item2') }}</h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p>
                                            {{ trans('about.about-item2-desc') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="featured-item text-center style-2 mx-3 my-3" style="min-height: 605px">
                                    <div class="featured-icon">
                                        <img class="img-fluid" src="images/feature/01.png" alt="">
                                    </div>
                                    <div class="featured-title">
                                        <h5>{{ trans('about.about-item3') }}</h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p>
                                            {{ trans('about.about-item3-desc') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overflow-hidden dark-bg custom-pb-18 animatedBackground" style="margin-bottom: 80px" data-bg-img="images/pattern/about.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title mb-0">
                            <!-- <h6>SUCCESS STORIES</h6> -->
                            <h2 class="title">{{ trans('tentang.tentang-vision') }}</h2>
                            <p class="mb-0 text-white">
                                {{ trans('tentang.tentang-vision-desc') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="light-bg overflow-hidden">
            <div class="container">
                <div class="section-title text-center">
                    {{-- <h6 class="abotus">{{ trans('about.about-label') }}</h6> --}}
                    <h2 class="title ">Our Client</h2>
                </div>
                <p class="text-black" style="font-weight: 300;">
                     Berikut beberapa klien kami yang telah merasakan dampak positif, peningkatan penjualan produk, peningkatan kepercayaan konsumen, dan peningkatan akses ke market baru selama bekerja sama dengan Yokesen.
                </p>
                <div style="height: 30px">

                </div>
                <div class="row mt-7 justify-content-center">
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Acaraki.png') }}
                        @endslot
                        @slot('name')
                            Acaraki
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/ACLUB.png') }}
                        @endslot
                        @slot('name2')
                            A-Club
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Bagus.png') }}
                        @endslot
                        @slot('name')
                            Bagus
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Bardi.png') }}
                        @endslot
                        @slot('name2')
                            Bardi
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Bayer.png') }}
                        @endslot
                        @slot('name')
                            Bayer
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/BBR.jpg') }}
                        @endslot
                        @slot('name2')
                            BBR
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Delicute.png') }}
                        @endslot
                        @slot('name')
                            Delicute
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Dermaviduals.jpg') }}
                        @endslot
                        @slot('name2')
                            Dermaviduals
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/DJP.jpg') }}
                        @endslot
                        @slot('name')
                            DJP
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Etoro.png') }}
                        @endslot
                        @slot('name2')
                            Etoro
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Financia.png') }}
                        @endslot
                        @slot('name')
                            Financia
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/iHoney.png') }}
                        @endslot
                        @slot('name2')
                            iHoney
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/IOMI.png') }}
                        @endslot
                        @slot('name')
                            IOMI
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/jakpro.png') }}
                        @endslot
                        @slot('name2')
                            JakPro
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/KopGron.png') }}
                        @endslot
                        @slot('name')
                            Kopi Grontol
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Lasegar.png') }}
                        @endslot
                        @slot('name2')
                            Lasegar
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/LPCB.png') }}
                        @endslot
                        @slot('name')
                            Larutan Penyegar Cap Badak
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Maxco.png') }}
                        @endslot
                        @slot('name2')
                            Maxco
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Melandas.jpg') }}
                        @endslot
                        @slot('name')
                            Melandas
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/OBIDA.png') }}
                        @endslot
                        @slot('name2')
                            Obida
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/orbi.png') }}
                        @endslot
                        @slot('name')
                            Orbitrade
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Pelindo.jpg') }}
                        @endslot
                        @slot('name2')
                            Pelindo
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Siangpure.png') }}
                        @endslot
                        @slot('name')
                            Siangpure
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Sweet Monsta.png') }}
                        @endslot
                        @slot('name2')
                            Sweet Monsta
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/taco.png') }}
                        @endslot
                        @slot('name')
                            TACO
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Telkom.png') }}
                        @endslot
                        @slot('name2')
                            Telkomsel
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/WGM.png') }}
                        @endslot
                        @slot('name')
                            Warisan Gajah Mada
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/WHW.png') }}
                        @endslot
                        @slot('name2')
                            Warisan Hayam Wuruk
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/WS.png') }}
                        @endslot
                        @slot('name')
                            Warisan Sriwijaya
                        @endslot
                        @slot('url2')
                        {{ asset('images/portofolio/TWG.png') }}
                        @endslot
                        @slot('name2')
                            TWG
                        @endslot
                    @endcomponent
                </div>
            </div>


        </section>
        <!--about end-->

        <!--Services start-->

        {{-- <section class="bg-contain bg-pos-r pt-0" data-bg-img="images/bg/right.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="section-title">
                            <h6 class="abotus">{{ trans('service.service-label') }}</h6>
                            <h2 class="title">
                                {{ trans('service.service-desc') }}
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel" data-items="1" data-autoplay="true">
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/services/01.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-caption">
                                                    <h5>{{ trans('service.service-item1') }}</h5>
                                                </div>
                                                <p>
                                                    {{ trans('service.service-item1-desc') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/services/02.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-caption">
                                                    <h5>{{ trans('service.service-item2') }}</h5>
                                                </div>
                                                <p>
                                                    {{ trans('service.service-item2-desc') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/services/03.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-caption">
                                                    <h5>{{ trans('service.service-item3') }}</h5>
                                                </div>
                                                <p>
                                                    {{ trans('service.service-item3-desc') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/services/04.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-caption">
                                                    <h5>{{ trans('service.service-item4') }}</h5>
                                                </div>
                                                <p>
                                                    {{ trans('service.service-item4-desc') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/services/05.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-caption">
                                                    <h5>{{ trans('service.service-item5') }}</h5>
                                                </div>
                                                <p>
                                                    {{ trans('service.service-item5-desc') }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--Services end-->

        <!--client logo start-->
        <!--<section>-->
        <!--    <div class="container">-->
        <!--        <div class="row">-->
        <!--            <div class="col-md-12">-->
        <!--                <div class="ht-clients d-flex flex-wrap align-items-center text-center">-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/bayer.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/delicute.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/lpcb.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/wgm.png" alt="">-->
        <!--                    </div>-->
        <!--                    {{-- <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/11.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/12.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/12.png" alt="">-->
        <!--                    </div>-->
        <!--                    <div class="clients-logo">-->
        <!--                        <img class="img-fluid" src="images/client/12.png" alt="">-->
        <!--                    </div> --}}-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</section>-->
        <!--client logo end-->


        <!--case studies start-->

        {{-- <section class="overflow-hidden dark-bg custom-pb-18 animatedBackground" data-bg-img="images/pattern/06.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title mb-0">
                            <h6>SUCCESS STORIES</h6>
                            <h2 class="title">Our Case Study</h2>
                            <p class="mb-0 text-white">Deos et accusamus et iusto odio dignissimos qui blanditiis
                                praesentium voluptatum dele corrupti quos dolores et quas molestias.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overflow-hidden pt-0 custom-mt-10 position-relative z-index-1">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="owl-carousel owl-center" data-items="4" data-md-items="2" data-sm-items="2"
                            data-center="true" data-dots="false" data-nav="true" data-autoplay="true">
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/01.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">Digital Marketing</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/02.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">Web & Mobile Apps</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/03.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">Traffic Management</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/04.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">Seo Optimization</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/05.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">Marketing Financing</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cases-item">
                                    <div class="cases-images">
                                        <img class="img-fluid" src="images/case-studies/06.jpg" alt="">
                                    </div>
                                    <div class="cases-description">
                                        <h5><a href="case-studies-single.html">SEO Analiysis</a></h5>
                                        <span>Digital</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--case studies end-->


        <!--content start-->

        {{-- <section class="light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12 order-lg-1">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-block">
                                    <img class="img-fluid box-shadow radius w-100" src="images/about/04.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 mt-4">
                                <div class="image-block">
                                    <img class="img-fluid box-shadow radius w-100" src="images/about/05.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 sm-mt-3">
                                <div class="image-block">
                                    <img class="img-fluid box-shadow radius w-100" src="images/about/06.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 mt-4">
                                <div class="image-block">
                                    <img class="img-fluid box-shadow radius w-100" src="images/about/07.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                        <div class="section-title mb-3">
                            <h6>What We Do</h6>
                            <h2>We iusto Creative Digital Agency, We Provide professional Web page.</h2>
                        </div>
                        <p class="text-black">Deos et accusamus et iusto odio dignissimos qui blanditiis praesentium
                            voluptatum dele corrupti quos dolores et quas molestias a orci facilisis rutrum.</p>
                        <ul class="custom-li list-unstyled list-icon-2 my-3 d-inline-block">
                            <li>Design must be functional</li>
                            <li>Futionality must into</li>
                            <li>Aenean pellentes vitae</li>
                            <li>Mattis effic iturut magna</li>
                            <li>Lusce enim nulla mollis</li>
                            <li>Phasellus eget felis</li>
                        </ul> <a class="btn btn-theme" href="#"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--content end-->


        <!--video start-->

        {{-- <section>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="round-animation">
                            <div class="video-box">
                                <img class="img-fluid" src="images/about/08.png" alt="">
                                <div class="video-btn video-btn-pos"> <a
                                        class="play-btn popup-youtube d-flex align-items-center"
                                        href="https://www.youtube.com/watch?v=P_wKDMcr1Tg"><span class="btn btn-white">Play
                                            Now</span><img class="img-fluid pulse radius-4" src="images/play.png"
                                            alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                        <div class="section-title mb-3">
                            <h6>What We Do</h6>
                            <h2>We Provide best Digital Marketing Solution.</h2>
                        </div>
                        <p class="text-black">Deos et accusamus et iusto odio dignissimos qui blanditiis praesentium
                            voluptatum dele corrupti quos dolores et quas molestias a orci facilisis rutrum.</p>
                        <ul class="custom-li list-unstyled list-icon-2 my-3 d-inline-block">
                            <li>Design must be functional</li>
                            <li>Futionality must into</li>
                            <li>Aenean pellentes vitae</li>
                            <li>Mattis effic iturut magna</li>
                            <li>Lusce enim nulla mollis</li>
                            <li>Phasellus eget felis</li>
                        </ul> <a class="btn btn-theme" href="#"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--video start-->





        <!--client logo start-->

        {{-- <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ht-clients d-flex flex-wrap align-items-center text-center">
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/07.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/08.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/09.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/10.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/11.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/12.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/12.png" alt="">
                            </div>
                            <div class="clients-logo">
                                <img class="img-fluid" src="images/client/12.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--client logo end-->


        <!--tab start-->

        {{-- <section class="light-bg animatedBackground" data-bg-img="images/pattern/05.png">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab style-2 ">
                            <!-- Nav tabs -->
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist"> <a class="nav-link active"
                                        id="nav-tab1" data-bs-toggle="tab" href="#tab1-1" role="tab"
                                        aria-selected="true">Marketing</a>
                                    <a class="nav-link" id="nav-tab2" data-bs-toggle="tab" href="#tab1-2" role="tab"
                                        aria-selected="false">Planning</a>
                                    <a class="nav-link" id="nav-tab3" data-bs-toggle="tab" href="#tab1-3" role="tab"
                                        aria-selected="false">Anlysis</a>
                                    <a class="nav-link" id="nav-tab4" data-bs-toggle="tab" href="#tab1-4" role="tab"
                                        aria-selected="false">Stratergy</a>
                                </div>
                            </nav>
                            <!-- Tab panes -->
                            <div class="tab-content" id="nav-tabContent">
                                <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <img class="img-fluid" src="images/about/02.png" alt="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                                            <h4 class="title">Powerful & Awesome Marketing</h4>
                                            <p>Simply dummy text of the printing and typesetting industry. standard
                                                dummy text ever since the 1500s, when an unknown printer took a
                                                galley of type and scrambled it to make a type specimen book.</p>
                                            <ul class="list-unstyled list-icon">
                                                <li class="mb-3"><i class="flaticon-tick"></i> Mattis effic iturut
                                                    magna pelle ntesque sit</li>
                                                <li class="mb-3"><i class="flaticon-tick"></i> Phasellus eget purus
                                                    id felis dignissim convallis</li>
                                                <li><i class="flaticon-tick"></i> Fusce enim nulla mollis eu metus
                                                    in sagittis fringilla</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab1-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <img class="img-fluid" src="images/about/02.png" alt="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                                            <h4 class="title">Powerful & Awesome Marketing</h4>
                                            <p>Simply dummy text of the printing and typesetting industry. standard
                                                dummy text ever since the 1500s, when an unknown printer took a
                                                galley of type and scrambled it to make a type specimen book.</p>
                                            <ul class="list-unstyled list-icon">
                                                <li class="mb-3"><i class="flaticon-tick"></i> Mattis effic iturut
                                                    magna pelle ntesque sit</li>
                                                <li class="mb-3"><i class="flaticon-tick"></i> Phasellus eget purus
                                                    id felis dignissim convallis</li>
                                                <li><i class="flaticon-tick"></i> Fusce enim nulla mollis eu metus
                                                    in sagittis fringilla</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab1-3">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <img class="img-fluid" src="images/about/02.png" alt="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                                            <h4 class="title">Powerful & Awesome Marketing</h4>
                                            <p>Simply dummy text of the printing and typesetting industry. standard
                                                dummy text ever since the 1500s, when an unknown printer took a
                                                galley of type and scrambled it to make a type specimen book.</p>
                                            <ul class="list-unstyled list-icon">
                                                <li class="mb-3"><i class="flaticon-tick"></i> Mattis effic iturut
                                                    magna pelle ntesque sit</li>
                                                <li class="mb-3"><i class="flaticon-tick"></i> Phasellus eget purus
                                                    id felis dignissim convallis</li>
                                                <li><i class="flaticon-tick"></i> Fusce enim nulla mollis eu metus
                                                    in sagittis fringilla</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab1-4">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <img class="img-fluid" src="images/about/02.png" alt="">
                                        </div>
                                        <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                                            <h4 class="title">Powerful & Awesome Marketing</h4>
                                            <p>Simply dummy text of the printing and typesetting industry. standard
                                                dummy text ever since the 1500s, when an unknown printer took a
                                                galley of type and scrambled it to make a type specimen book.</p>
                                            <ul class="list-unstyled list-icon">
                                                <li class="mb-3"><i class="flaticon-tick"></i> Mattis effic iturut
                                                    magna pelle ntesque sit</li>
                                                <li class="mb-3"><i class="flaticon-tick"></i> Phasellus eget purus
                                                    id felis dignissim convallis</li>
                                                <li><i class="flaticon-tick"></i> Fusce enim nulla mollis eu metus
                                                    in sagittis fringilla</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--tab end-->


        <!--team start-->

        {{-- <section class="bg-contain" data-bg-img="images/pattern/02.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title">
                            <h6>Creative Team</h6>
                            <h2 class="title">Meet Our Expert team member will ready for your service</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="team-member">
                            <div class="team-images">
                                <img class="img-fluid" src="images/team/01.jpg" alt="">
                            </div>
                            <div class="team-description"> <span>Manager</span>
                                <h5><a href="team-single.html">Jemy Lusto</a></h5>
                                <div class="team-social-icon">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="team-member active">
                            <div class="team-images">
                                <img class="img-fluid" src="images/team/02.jpg" alt="">
                            </div>
                            <div class="team-description"> <span>Manager</span>
                                <h5><a href="team-single.html">Jemy Lusto</a></h5>
                                <div class="team-social-icon">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="team-member">
                            <div class="team-images">
                                <img class="img-fluid" src="images/team/03.jpg" alt="">
                            </div>
                            <div class="team-description"> <span>Manager</span>
                                <h5><a href="team-single.html">Jemy Lusto</a></h5>
                                <div class="team-social-icon">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        </li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--team end-->


        <!--testimonial start-->

        {{-- <section class="bg-contain bg-pos-r pt-0" data-bg-img="images/bg/02.png">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title">
                            <h6>Testimonial</h6>
                            <h2 class="title">You Can See our clients feedback What You Say?</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel" data-items="1" data-autoplay="true">
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/testimonial/01.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-quote"><i class="flaticon-quotation"></i>
                                                </div>
                                                <p>Professional recommended and great experience, Nam pulvinar vitae
                                                    neque et porttitor, Praesent sed nisi eleifend, Consectetur
                                                    adipisicing elit, sed do eiusmodas temporo incididunt</p>
                                                <div class="testimonial-caption">
                                                    <h5>Lana Roadse</h5>
                                                    <label>CEO of Loptus</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/testimonial/01.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-quote"><i class="flaticon-quotation"></i>
                                                </div>
                                                <p>Professional recommended and great experience, Nam pulvinar vitae
                                                    neque et porttitor, Praesent sed nisi eleifend, Consectetur
                                                    adipisicing elit, sed do eiusmodas temporo incididunt</p>
                                                <div class="testimonial-caption">
                                                    <h5>Lana Roadse</h5>
                                                    <label>CEO of Loptus</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial style-2">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="testimonial-img info-img round-animation">
                                                <img class="img-fluid leftRight" src="images/testimonial/01.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-12 ms-auto mt-5 mt-lg-0">
                                            <div class="testimonial-content">
                                                <div class="testimonial-quote"><i class="flaticon-quotation"></i>
                                                </div>
                                                <p>Professional recommended and great experience, Nam pulvinar vitae
                                                    neque et porttitor, Praesent sed nisi eleifend, Consectetur
                                                    adipisicing elit, sed do eiusmodas temporo incididunt</p>
                                                <div class="testimonial-caption">
                                                    <h5>Lana Roadse</h5>
                                                    <label>CEO of Loptus</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--testimonial end-->


        <!--price table start-->

        {{-- <section class="light-bg">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title">
                            <h6>Price Table</h6>
                            <h2 class="title">Choose Your Pricing plan</h2>
                            <p class="mb-0">Deos et accusamus et iusto odio dignissimos qui blanditiis praesentium
                                voluptatum dele corrupti quos dolores et quas molestias.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="price-table style-2 bg-contain bg-pos-r" data-bg-img="images/bg/02.png">
                            <div class="price-header d-md-flex justify-content-between">
                                <h3 class="price-title">Starter</h3>
                                <div class="price-value">
                                    <h2>$29<span>/Month</span></h2>
                                </div>
                            </div>
                            <div class="price-list">
                                <ul class="list-unstyled custom-li">
                                    <li><i class="flaticon-tick"></i> 15 Analytics Compaign</li>
                                    <li><i class="flaticon-tick"></i> Unlimited Site licenses</li>
                                    <li><i class="flaticon-tick"></i> 1 Database</li>
                                    <li><i class="flaticon-tick"></i> 10 Free Optimization</li>
                                    <li><i class="flaticon-tick"></i> Html5 + Css3</li>
                                    <li><i class="flaticon-tick"></i> 24/7 Customer Support</li>
                                </ul>
                            </div>
                            <a class="btn btn-theme" href="#"> <span>Get Started</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 mt-5 mt-lg-0">
                        <div class="price-table style-2 bg-contain bg-pos-r" data-bg-img="images/bg/02.png">
                            <div class="price-header d-md-flex justify-content-between">
                                <h3 class="price-title">Premium</h3>
                                <div class="price-value">
                                    <h2>$99<span>/Month</span></h2>
                                </div>
                            </div>
                            <div class="price-list">
                                <ul class="list-unstyled custom-li">
                                    <li><i class="flaticon-tick"></i> 15 Analytics Compaign</li>
                                    <li><i class="flaticon-tick"></i> Unlimited Site licenses</li>
                                    <li><i class="flaticon-tick"></i> 1 Database</li>
                                    <li><i class="flaticon-tick"></i> 10 Free Optimization</li>
                                    <li><i class="flaticon-tick"></i> Html5 + Css3</li>
                                    <li><i class="flaticon-tick"></i> 24/7 Customer Support</li>
                                </ul>
                            </div>
                            <a class="btn btn-theme" href="#"> <span>Get Started</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 mt-5">
                        <div class="price-table style-2 bg-contain bg-pos-r" data-bg-img="images/bg/02.png">
                            <div class="price-header d-md-flex justify-content-between">
                                <h3 class="price-title">Professional</h3>
                                <div class="price-value">
                                    <h2>$199<span>/Month</span></h2>
                                </div>
                            </div>
                            <div class="price-list">
                                <ul class="list-unstyled custom-li">
                                    <li><i class="flaticon-tick"></i> 15 Analytics Compaign</li>
                                    <li><i class="flaticon-tick"></i> Unlimited Site licenses</li>
                                    <li><i class="flaticon-tick"></i> 1 Database</li>
                                    <li><i class="flaticon-tick"></i> 10 Free Optimization</li>
                                    <li><i class="flaticon-tick"></i> Html5 + Css3</li>
                                    <li><i class="flaticon-tick"></i> 24/7 Customer Support</li>
                                </ul>
                            </div>
                            <a class="btn btn-theme" href="#"> <span>Get Started</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 mt-5">
                        <div class="price-table style-2 bg-contain bg-pos-r" data-bg-img="images/bg/02.png">
                            <div class="price-header d-md-flex justify-content-between">
                                <h3 class="price-title">Standard</h3>
                                <div class="price-value">
                                    <h2>$299<span>/Month</span></h2>
                                </div>
                            </div>
                            <div class="price-list">
                                <ul class="list-unstyled custom-li">
                                    <li><i class="flaticon-tick"></i> 15 Analytics Compaign</li>
                                    <li><i class="flaticon-tick"></i> Unlimited Site licenses</li>
                                    <li><i class="flaticon-tick"></i> 1 Database</li>
                                    <li><i class="flaticon-tick"></i> 10 Free Optimization</li>
                                    <li><i class="flaticon-tick"></i> Html5 + Css3</li>
                                    <li><i class="flaticon-tick"></i> 24/7 Customer Support</li>
                                </ul>
                            </div>
                            <a class="btn btn-theme" href="#"> <span>Get Started</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--price table end-->


        <!--blog start-->

        {{-- <section>
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="section-title">
                            <h6>Blog</h6>
                            <h2 class="title">Read our latest news</h2>
                            <p class="mb-0">Deos et accusamus et iusto odio dignissimos qui blanditiis praesentium
                                voluptatum dele corrupti quos dolores et quas molestias.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="post style-2">
                            <div class="post-image">
                                <img class="img-fluid h-100 w-100" src="images/blog/01.jpg" alt="">
                            </div>
                            <div class="post-desc">
                                <div class="post-date">23 <span>November 2018</span>
                                </div>
                                <div class="post-title">
                                    <h5><a href="blog-details.html">The Powerfull look for best in 2018</a></h5>
                                </div>
                                <p>Phasellus eget purus id felis dignissim convallis Suspendisse et augue dui
                                    gravida</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="post style-2">
                            <div class="post-image">
                                <img class="img-fluid h-100 w-100" src="images/blog/02.jpg" alt="">
                            </div>
                            <div class="post-desc">
                                <div class="post-date">23 <span>November 2018</span>
                                </div>
                                <div class="post-title">
                                    <h5><a href="blog-details.html">Loptus It's Awesome, We need your help</a></h5>
                                </div>
                                <p>Phasellus eget purus id felis dignissim convallis Suspendisse et augue dui
                                    gravida</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0">
                        <div class="post style-2">
                            <div class="post-image">
                                <img class="img-fluid h-100 w-100" src="images/blog/03.jpg" alt="">
                            </div>
                            <div class="post-desc">
                                <div class="post-date">23 <span>November 2018</span>
                                </div>
                                <div class="post-title">
                                    <h5><a href="blog-details.html">We Become Best sale marketer in a year!</a></h5>
                                </div>
                                <p>Phasellus eget purus id felis dignissim convallis Suspendisse et augue dui
                                    gravida</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!--blog end-->

    </div>

    <!--body content end-->
@endsection
