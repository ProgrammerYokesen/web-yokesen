@extends('app')

@section('additional_assets')
    <link rel="stylesheet" href="{{ asset('css/custom/landing.css') }}?v=2">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/keen-slider@latest/keen-slider.min.css" />

@endsection

@section('content')
    <section class="page-title overflow-hidden landing__tcw_hero">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8">
                    <h5>TERATUR CLOUD WAREHOUSE</h5>
                    <h1>Terkendala memproses orderan dan pengiriman?</h1>
                </div>
                <div class="col-lg-4">
                    <form id="regis__form" class="row g-4 needs-validation" method="post"
                        action="{{ route('registerData') }}">
                        @csrf
                        {{-- <h6>Daftarkan dirimu disini</h6> --}}
                        <div class="col-md-12">
                            <input id="form_name" type="text" name="name" class="form-control" placeholder="Name"
                                autocomplete="off" required>
                        </div>
                        <div class="col-md-12">
                            <input id="form_email" type="email" name="email" class="form-control" placeholder="Email"
                                autocomplete="off" required>
                        </div>
                        <div class="col-md-12">
                            <input id="form_phone" type="tel" name="phone" class="form-control"
                                placeholder="Nomor Whatsapp" autocomplete="off" required>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn-theme btn-radius"><span>Ayo mulai, GRATIS!</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <img src="{{ asset('images/landing/wave-top.png') }}" alt="">

    </section>

    <div class="page-content mb-10" style="position: relative">
        <div class="container">

            <div class="landing__tcw_info top_card">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>Apakah ini situasi yang sedang Anda hadapi?</h2>
                        <h5>Menghabiskan waktu berjam-jam dalam sehari untuk pengemasan (packing), mencetak berlembar-lembar
                            resi dan label, dan bolak-balik kontak dengan kurir pengiriman</h5>
                        <h5>Belum sanggup menambah infrastruktur seperti pembelian rak untuk stok barang atau gudang untuk
                            menyimpan stok dagangan</h5>
                        <h5>Terkendala dalam mengatur stok barang dan pencatatan</h5>
                        <h4>Dengan TERATUR, semua orderan beres dalam waktu 5 menit!</h4>
                    </div>
                </div>
            </div>

            <div class="landing__tcw_info">
                <div class="card">
                    <div class="card-body text-center">
                        <h2>Apa itu Teratur Cloud Warehouse (TCW)?</h2>
                        <h5>Teratur adalah platform untuk mempersingkat proses penyimpanan, pengemasan, dan pengiriman
                            pesanan secara efektif dari toko online di berbagai e-commerce di Indonesia</h5>
                        {{-- <a href="#" class="tcw__gold">Pelajari lebih lanjut mengenai Teratur Cloud Warehouse <i
                                class="fas fa-arrow-right"></i> --}}
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <img src="{{ asset('images/landing/wave-mid.png') }}" alt="" class="img__orn_mid">
    </div>

    <div class="page-content mb-10">
        <div class="container">
            <div class="landing__tcw_desc">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <img src="{{ asset('images/landing/illustration1.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <h2>Pemrosesan pesanan yang kurang dari 5 menit</h2>
                        <h5>Pesanan secara otomatis dikirim ke sistem gudang fulfilment kami yang paling dekat dengan lokasi
                            pembeli, dan proses otomatisasi proses orderan dimulai dalam waktu tidak sampai 5 menit barang
                            siap dikirim ke pembeli!
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content mb-10">
        <div class="container">
            <div class="landing__tcw_desc">
                <div class="row align-items-center">

                    <div class="col-lg-7 orderr-1">
                        <h2>Sinkronisasi dan Agregasi stok ke berbagai toko dan platform ecommerce di Indonesia</h2>
                        <h5>Memudahkan untuk memantau stok barang, pergerakan stok masuk dan keluar, melihat status pesanan
                            secara real-time, data historis yang berguna untuk perencanaan stok maupun promosi, dan banyak
                            lagi.
                        </h5>
                    </div>
                    <div class="col-lg-5 orderr-2">
                        <img src="{{ asset('images/landing/illustration2.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content mb-10 landing__tcw_benefit" style="overflow: hidden">
        {{-- <div class="container"> --}}
        <h3 class="mb-10">KEUNTUNGAN TCW</h3>

        <div id="benefit__slider" class="keen-slider">

            <div class="keen-slider__slide number-slide1">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing/benefit1.png') }}" alt="">
                        <h5>Sistem otomatis untuk proses pesanan toko online anda
                            dari banyak ecommerce </h5>
                        <p>Tak perlu lagi menunggu admin merespon setiap orderan yang masuk. Sistem TERATUR secara langsung
                            memproses pesanan dalam waktu 5 menit saja tanpa proses input manual!</p>
                    </div>
                </div>
            </div>
            <div class="keen-slider__slide number-slide1">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing/benefit2.png') }}" alt="">

                        <h5>Waktu Pengiriman yang Super Cepat</h5>
                        <p>Jika Anda memproses ratusan pesanan atau lebih setiap harinya, dengan sistem TERATUR hanya
                            dibutuhkan waktu 5 menit mulai dari terjadi orderan pembeli hingga ke tahap siap dijemput kurir.
                        </p>
                    </div>
                </div>
            </div>
            <div class="keen-slider__slide number-slide1">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing/benefit3.png') }}" alt="">

                        <h5>Anda bisa mulai menerima pesanan konsumen dari banyak area secara bersamaan</h5>
                        <p>Dengan otomatisasi, tim Anda dapat memproses pesanan dari mana saja dengan melacak pesanan secara
                            real-time dari sistem dan orderan akan dikirim secara otomatis ke gudang stok barang yang paling
                            dekat dengan lokasi pembeli.</p>
                    </div>
                </div>
            </div>
            <div class="keen-slider__slide number-slide1">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing/benefit4.png') }}" alt="">

                        <h5>Dengan TERATUR, akan mengurangi biaya untuk pelayanan dan fulfilment</h5>
                        <p>Tak perlu lagi banyak admin untuk memproses dan menginput pesanan dari setiap toko online di
                            banyak e-commerce
                            dan tidak akan terjadi kesalahan input data</p>
                    </div>
                </div>
            </div>
            <div class="keen-slider__slide number-slide1">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('images/landing/benefit5.png') }}" alt="">

                        <h5>Lacak status pesanan secara Real-time</h5>
                        <p>Terintegrasi dengan sistem manajemen stok dan sistem manajemen gudang, yang memberi Anda kendali
                            penuh memnatau data transaksi dan stok.
                            Semua pesanan bisa dilacak di satu dasbor, update apapun secara real-time, semuanya di satu
                            sistem.
                        </p>
                    </div>
                </div>
            </div>

        </div>
        {{-- </div> --}}
    </div>

    <div class="page-content mb-10 landing__tcw_testi">
        <div class="container">
           <div class="testi__wrapper">
                <div class="landing__tcw_testi_title">
                    APA KATA MEREKA
                </div>
    
                <h2>Cukup 5 menit untuk proses orderan dengan TERATUR. Sisa waktu bisa dimanfaatkan untuk bikin strategi
                    pengembangan toko.</h2>
    
                <div class="landing__tcw_testi_people">
                    <img src="{{ asset('images/landing/testi_user.png') }}" alt="">
                    <div style="margin-left: 15px">
                        <h5>Shinta</h5>
                        <h6>Jr. Operational Manager</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content mb-10 landing__tcw_footer">
        <div class="container">
            <h1>Kembangkan Bisnismu bersama</h1>
            <h1>Teratur Cloud Warehouse</h1>

            <div class="text-center">
                <button class="btn__daftar">Ayo mulai, GRATIS!</button>
            </div>
        </div>
    </div>
@endsection

@section('asset_js')
    <script src="https://cdn.jsdelivr.net/npm/keen-slider@latest/keen-slider.js"></script>
    {{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.all.min.js"
        integrity="sha256-sq9BgeqJAlCLfjfAO+2diFGt5O8aIYNrds+yhlpFnvg=" crossorigin="anonymous"></script>

    <script>
        var slider = new KeenSlider("#benefit__slider", {
            slidesPerView: 2.5,
            mode: "free-snap",
            spacing: 15,
            centered: true,
            loop: false,
            breakpoints: {
                "(max-width: 1200px)": {
                    slidesPerView: 2.5,
                    mode: "free-snap",
                },
                "(max-width: 768px)": {
                    slidesPerView: 1.15,
                    mode: "free-snap",
                },
            },
        })
    </script>

    <script>
        $(".btn__daftar").click(function() {
            $('html,body').animate({
                    scrollTop: $("#regis__form").offset().top - 100
                },
                'slow');
        });
    </script>


    @if (Session::has('status'))
        <script>
            Swal.fire({
                title: 'Terima kasih sudah mengambil keputusan yang tepat',
                text: 'Selanjutnya tim kami akan menghubungi Anda untuk proses lebih lanjut. Diharapkan untuk bersedia dihubungi melalui Nomor Whatsapp yang sudah disubmit melalui form.',
                width: 600,
                icon: 'success'
            })
        </script>
    @endif

@endsection
