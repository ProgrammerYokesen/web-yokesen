@extends('app')
@section('meta_tag')
    {{-- <meta name="description"
        content="{{ trans('digital.digital-meta-desc') }}" /> --}}
    <meta name="description" content="Klien dari Yokesen" />
    <meta name="keywords" content="klien, portofolio" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | Our Client
@endsection

@section('additional_assets')
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">
    <link rel="stylesheet" href="{{ asset(generateCSS('custom/new-lp')) }}">

@endsection

@section('content')
    <!--page title start-->

    <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">Our Client</h1>
                </div>
            </div>
        </div>
    </section>

    <!--page title end-->
    
    <!--service start-->
    <div class="page-content">
        <section class="light-bg overflow-hidden">
            <div class="container">
                <p class="text-black" style="font-weight: 300;">
                     Berikut beberapa klien kami yang telah merasakan dampak positif, peningkatan penjualan produk, peningkatan kepercayaan konsumen, dan peningkatan akses ke market baru selama bekerja sama dengan Yokesen.

                </p>
                <div style="height: 30px">

                </div>
                <div class="row mt-7 justify-content-center">
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Acaraki.png') }}
                        @endslot
                        @slot('name')
                            Acaraki
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/ACLUB.png') }}
                        @endslot
                        @slot('name2')
                            A-Club
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Bagus.png') }}
                        @endslot
                        @slot('name')
                            Bagus
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Bardi.png') }}
                        @endslot
                        @slot('name2')
                            Bardi
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Bayer.png') }}
                        @endslot
                        @slot('name')
                            Bayer
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/BBR.jpg') }}
                        @endslot
                        @slot('name2')
                            BBR
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Delicute.png') }}
                        @endslot
                        @slot('name')
                            Delicute
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Dermaviduals.jpg') }}
                        @endslot
                        @slot('name2')
                            Dermaviduals
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/DJP.jpg') }}
                        @endslot
                        @slot('name')
                            DJP
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Etoro.png') }}
                        @endslot
                        @slot('name2')
                            Etoro
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Financia.png') }}
                        @endslot
                        @slot('name')
                            Financia
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/iHoney.png') }}
                        @endslot
                        @slot('name2')
                            iHoney
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/IOMI.png') }}
                        @endslot
                        @slot('name')
                            IOMI
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/jakpro.png') }}
                        @endslot
                        @slot('name2')
                            JakPro
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/KopGron.png') }}
                        @endslot
                        @slot('name')
                            Kopi Grontol
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Lasegar.png') }}
                        @endslot
                        @slot('name2')
                            Lasegar
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/LPCB.png') }}
                        @endslot
                        @slot('name')
                            Larutan Penyegar Cap Badak
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Maxco.png') }}
                        @endslot
                        @slot('name2')
                            Maxco
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Melandas.jpg') }}
                        @endslot
                        @slot('name')
                            Melandas
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/OBIDA.png') }}
                        @endslot
                        @slot('name2')
                            Obida
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/orbi.png') }}
                        @endslot
                        @slot('name')
                            Orbitrade
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Pelindo.jpg') }}
                        @endslot
                        @slot('name2')
                            Pelindo
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/Siangpure.png') }}
                        @endslot
                        @slot('name')
                            Siangpure
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Sweet Monsta.png') }}
                        @endslot
                        @slot('name2')
                            Sweet Monsta
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/taco.png') }}
                        @endslot
                        @slot('name')
                            TACO
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/Telkom.png') }}
                        @endslot
                        @slot('name2')
                            Telkomsel
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/WGM.png') }}
                        @endslot
                        @slot('name')
                            Warisan Gajah Mada
                        @endslot
                        @slot('url2')
                            {{ asset('images/portofolio/WHW.png') }}
                        @endslot
                        @slot('name2')
                            Warisan Hayam Wuruk
                        @endslot
                    @endcomponent
                    @component('components.event-lp.new-partner-box')
                        @slot('url')
                            {{ asset('images/portofolio/WS.png') }}
                        @endslot
                        @slot('name')
                            Warisan Sriwijaya
                        @endslot
                        @slot('url2')
                        {{ asset('images/portofolio/TWG.png') }}
                        @endslot
                        @slot('name2')
                            TWG
                        @endslot
                    @endcomponent
                </div>
            </div>


        </section>
    </div>
    <!--service end-->
@endsection
