@extends('app')
@section('meta_tag')
    <meta name="keywords" content="Yokesen, Contact Yokesen, Contact, contact, yokesen, contact yokesen" />
    <meta name="description" content="Contact Yokesen contact@yokesen.com" />
    <meta name="author" content="https://yokesen.com" />
@endsection

@section('title_tag')
    | Sitemap
@endsection

@section('content')
    <div class="container" style="margin-top:8rem">
        <div class="row align-items-center">
            <div class="col-md-12">
                <h1 class="title">Sitemap</h1>
            </div>
        </div>
    </div>

    <div class="page-content">
        <section class="form-info " style="padding:20px 0">
            <div class="container">
                <div class="contact-info" style="padding-left:2rem">
                    <div class="mb-3">
                        <a href="{{ route('homePage') }}">
                            <p class="mb-0 p-0 pb-1 title">Home</p>
                        </a>
                    </div>
                    <div class="mb-3">
                        <a href="{{ route('aboutPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Company</p>
                        </a>
                    </div>
                    <div class="mb-3">
                        <p class="mb-0 p-0 pb-1 " style="font-weight: 700">Services</p>
                    </div>
                    <div class="mb-3 ms-5">
                        <a href="{{ route('digitalPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Our Services</p>
                        </a>
                    </div>
                    <div class="mb-3 ms-5">
                        <a href="{{ route('thisThatPage') }}">
                            <p class="mb-0 p-0 pb-1 title">From That to This</p>
                        </a>
                    </div>
                    <div class="mb-3 ms-5">
                        <a href="{{ route('gamificationPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Gamification</p>
                        </a>
                    </div>
                    <div class="mb-3">
                        <a href="{{ route('blogPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Blog</p>
                        </a>
                    </div>
                    <div class="mb-3">
                        <a href="{{ route('webinarPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Webinar</p>
                        </a>
                    </div>
                    <div class="mb-3">
                        <a href="{{ route('contactPage') }}">
                            <p class="mb-0 p-0 pb-1 title">Contact Us</p>
                        </a>
                    </div>
                    
                </div>
        </section>
    </div>
@endsection
