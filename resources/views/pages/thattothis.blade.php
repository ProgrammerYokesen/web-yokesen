@extends('app')
@section('meta_tag')
    <meta name="description"
        content="{{ trans('that-this.that-this-meta-desc') }}" />
    <meta name="keywords"
        content="{{ trans('that-this.that-this-meta-keyword') }}" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | From that to this
@endsection

@section('content')
    <!--page title start-->

    {{-- <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">
                        {{ trans('that-this.that-this-title') }}
                    </h1> 
                </div>
            </div>
        </div>
    </section> --}}

    <!--page title end-->

    <div style="height: 80px"></div>
    <section class="overflow-hidden">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-lg-8 col-md-12">
                    <div class="section-title">
                        <h6>{{ trans('that-this.that-this-title') }}</h6>
                        <h2 class="title">
                            {{ trans('that-this.that-this-exp') }}
                        </h2>
                        <p>
                            {{ trans('that-this.that-this-desc') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--service start-->

    <section class="position-relative overflow-hidden bg-contain bg-pos-l" data-bg-img="images/bg/left.png">
        <div class="container">

            <div class="accordion" id="accordion">
                {{-- Improved Growth --}}
                <div class="accordion-item mb-4" style="border: transparent">
                    <h2 class="accordion-header accordion-button title" id="headingOne" data-bs-toggle="collapse"
                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"
                        style="border: transparent">
                        {{ trans('that-this.that-growth') }}
                    </h2>
                    <div id="collapseOne" class="accordion-collapse border-0 collapse show" aria-labelledby="headingOne"
                        data-bs-parent="#accordion" style="border: transparent">
                        <div class="accordion-body">
                            <div class="row justify-content-center thattothis">
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-objective') }}</h6>
                                    <h4>{{ trans('that-this.that-growth') }}</h4>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-kpi') }}</h6>
                                    <h2>140%</h2>
                                    <p>{{ trans('that-this.that-growth-kpi') }}</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>{{ trans('that-this.that-result') }}</h6>
                                    <h2>69</h2>
                                    <p>
                                        {{ trans('that-this.that-growth-result') }}
                                    </p>
                                    <br>
                                    <h2>25</h2>
                                    <p>{{ trans('that-this.that-growth-result2') }}</p>
                                </div>
                                <div class="col-md-2">
                                    <h6>{{ trans('that-this.that-period') }}</h6>
                                    <h2>3</h2>
                                    <p>{{ trans('that-this.that-growth-period') }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



                {{-- Improved Performance --}}
                <div class="accordion-item mb-4" style="border: transparent">
                    <h2 class="accordion-header accordion-button title collapsed" id="headingTwo" data-bs-toggle="collapse"
                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"
                        style="border: transparent">
                        {{ trans('that-this.that-improved') }}
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse border-0 collapse" aria-labelledby="headingTwo"
                        data-bs-parent="#accordion" style="border: transparent">
                        <div class="accordion-body">
                            <div class="row justify-content-center thattothis">
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-objective') }}</h6>
                                    <h4>{{ trans('that-this.that-improved') }}</h4>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-kpi') }}</h6>
                                    <h2>300%</h2>
                                    <p>{{ trans('that-this.that-improved-kpi') }}</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>{{ trans('that-this.that-result') }}</h6>
                                    <h2>4.2%</h2>
                                    <p>
                                        {{ trans('that-this.that-improved-result') }}
                                    </p>
                                    <br>
                                    <h2>30k</h2>
                                    <p>{{ trans('that-this.that-improved-result2') }}</p>
                                </div>
                                <div class="col-md-2">
                                    <h6>{{ trans('that-this.that-period') }}</h6>
                                    <h2>30</h2>
                                    <p>{{ trans('that-this.that-improved-period') }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                {{-- Access To New Market --}}
                <div class="accordion-item mb-4" style="border: transparent">
                    <h2 class="accordion-header accordion-button title collapsed" id="headingThree"
                        data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false"
                        aria-controls="collapseThree" style="border: transparent">
                        {{ trans('that-this.that-access') }}
                    </h2>
                    <div id="collapseThree" class="accordion-collapse border-0 collapse" aria-labelledby="headingThree"
                        data-bs-parent="#accordion" style="border: transparent">
                        <div class="accordion-body">
                            <div class="row justify-content-center thattothis">
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-objective') }}</h6>
                                    <h4>{{ trans('that-this.that-access') }}</h4>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-kpi') }}</h6>
                                    <h2>4.2%</h2>
                                    <p>{{ trans('that-this.that-access-kpi') }}</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>{{ trans('that-this.that-result') }}</h6>
                                    <h2>10x</h2>
                                    <p>
                                        {{ trans('that-this.that-access-result') }}
                                    </p>
                                    <br>
                                    <h2>92%</h2>
                                    <p>{{ trans('that-this.that-access-result2') }}</p>
                                </div>
                                <div class="col-md-2">
                                    <h6>{{ trans('that-this.that-period') }}</h6>
                                    <h2>30</h2>
                                    <p>{{ trans('that-this.that-improved-period') }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                {{-- Lower Cost Of Doing Business And Sales --}}
                <div class="accordion-item mb-4" style="border: transparent">
                    <h2 class="accordion-header accordion-button title collapsed" id="headingFour" data-bs-toggle="collapse"
                        data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"
                        style="border: transparent">
                        Lower Cost Of Doing Business And Sales
                    </h2>
                    <div id="collapseFour" class="accordion-collapse border-0 collapse" aria-labelledby="headingFour"
                        data-bs-parent="#accordion" style="border: transparent">
                        <div class="accordion-body">
                            <div class="row justify-content-center thattothis">
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-objective') }}</h6>
                                    <h4>Lower Cost Of Doing Business And Sales</h4>
                                </div>
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-kpi') }}</h6>
                                    <h2>5x</h2>
                                    <p>lower CPR compared with industry average</p>
                                </div>
                                <div class="col-md-4">
                                    <h6>{{ trans('that-this.that-result') }}</h6>
                                    <h2>2x</h2>
                                    <p>
                                        Lower bounce rate compared to industry average
                                    </p>
                                    <br>
                                    <h2>92%</h2>
                                    <p>% of high quality leads per funneling</p>
                                </div>
                                <div class="col-md-2">
                                    <h6>{{ trans('that-this.that-period') }}</h6>
                                    <h2>3</h2>
                                    <p>{{ trans('that-this.that-growth-period') }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                {{-- Customer And Shopping Convenience And Immediate Delivery --}}
                <div class="accordion-item mb-4" style="border: transparent">
                    <h2 class="accordion-header accordion-button title collapsed" id="headingFive" data-bs-toggle="collapse"
                        data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"
                        style="border: transparent">
                        Customer And Shopping Convenience And Immediate Delivery
                    </h2>
                    <div id="collapseFive" class="accordion-collapse border-0 collapse" aria-labelledby="headingFive"
                        data-bs-parent="#accordion" style="border: transparent">
                        <div class="accordion-body">
                            <div class="row justify-content-center thattothis">
                                <div class="col-md-3">
                                    <h6>{{ trans('that-this.that-objective') }}</h6>
                                    <h4>Customer And Shopping Convenience And Immediate Delivery</h4>
                                </div>
                                <div class="col-md-3">
                                    <h6>Key Performance Indicator</h6>
                                    <h2>-</h2>
                                </div>
                                <div class="col-md-4">
                                    <h6>{{ trans('that-this.that-result') }}</h6>
                                    <h2>2x</h2>
                                    <p>Amount of Online retail stores handled in 3 Major Marketplaces in Indonesia
                                    </p>
                                    <br>
                                    <h2>25%</h2>
                                    <p>of Consumer Retention Rate</p>
                                </div>
                                <div class="col-md-2">
                                    <h6>{{ trans('that-this.that-period') }}</h6>
                                    <h2>3</h2>
                                    <p>Quartals</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>



    <!--service end-->


@endsection
