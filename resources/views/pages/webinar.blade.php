@extends('app')
@section('meta_tag')
    {{-- <meta name="description"
        content="{{ trans('digital.digital-meta-desc') }}" /> --}}
    <meta name="description" content="Klien dari Yokesen" />
    <meta name="keywords" content="klien, portofolio" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Yokesen Teknologi Indonesia" />
@endsection

@section('title_tag')
    | Webinar
@endsection

@section('additional_assets')
    <link rel="stylesheet" href="{{ asset(generateCSS('new-web')) }}">
    {{-- <link rel="stylesheet" href="{{ asset(generateCSS('custom/new-lp')) }}"> --}}

@endsection

@section('content')
    <!--page title start-->

    {{-- <section class="page-title overflow-hidden text-center light-bg bg-contain animatedBackground"
        data-bg-img="images/pattern/new.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">Our Client</h1>
                </div>
            </div>
        </div>
    </section> --}}

    <!--page title end-->


    <!--service start-->
    <div class="page-content">
        <section class="light-bg overflow-hidden">
            <div class="container">
                <div class="row mt-4">
                    <div class="col-md-9">
                        <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-bg mb-4">
                        <div class="card shadow border-0">
                            <div class="card-body">
                                <h4 class="title">Register</h4>
                                <form id="contact-form" class="row g-4 needs-validation" method="post"
                                    novalidate>
                                    <div class="messages"></div>
                                    <div class="col-md-6">
                                        <input id="form_name" type="text" name="name" class="form-control"
                                            placeholder="Name" required>
                                        <div class="invalid-feedback">Name is required.</div>
                                    </div>
                                    <div class="col-md-6">
                                        <input id="form_email" type="email" name="email" class="form-control"
                                            placeholder="Email" required>
                                        <div class="invalid-feedback">Valid email is required.</div>
                                    </div>
                                    <div class="col-md-12">
                                        <input id="form_phone" type="tel" name="phone" class="form-control"
                                            placeholder="Phone" required>
                                        <div class="invalid-feedback">Phone is required</div>
                                    </div>
                                   
                                    <div class="col-md-12">
                                        <button class="btn btn-theme btn-radius"><span>Register</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mt-md-0 mt-5">
                        <h5 class="text-muted font-weight-bold mb-5">Webinar Sebelumnya</h5>
                        <a href="">
                            <div class="mb-3">
                                <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-small">
                                <p class="text-black mb-0">Titledhahds </p>
                            </div>
                        </a>
                        
                        <a href="">
                            <div class="mb-3">
                                <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-small">
                                <p class="text-black mb-0">Titledhahds </p>
                            </div>
                        </a>
                        <a href="">
                            <div class="mb-3">
                                <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-small">
                                <p class="text-black mb-0">Titledhahds </p>
                            </div>
                        </a>
                        <a href="">
                            <div class="mb-3">
                                <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-small">
                                <p class="text-black mb-0">Titledhahds </p>
                            </div>
                        </a>
                        <a href="">
                            <div class="mb-3">
                                <img src="https://24slides.com/templates/upload/templates-previews/E4NCFLMAoDZxySbA0YPMlAd7cZjsCl9q6cV4xfj8.jpg" alt="" class="img-webinar-small">
                                <p class="text-black mb-0">Titledhahds </p>
                            </div>
                        </a>
                    </div>
                </div>


                
            </div>


        </section>
    </div>
    <!--service end-->
@endsection
