<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->


<url>
  <loc>https://yokesen.com/id/teknologi-dan-strategi-digital</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>1.00</priority>
</url>
<url>
  <loc>https://yokesen.com/id/company</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/our-services</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/that-to-this</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/our-client</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/gamification</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/webinar</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/contact-us</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/sitemap</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.80</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/apakah-safety-stock-penting-dalam-supply-chain-management</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/meningkatkan-brand-awareness-melalui-strategi-event-marketing</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-kol-marketing-untuk-tingkatkan-keuntungan-brand-kamu</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-digital-branding-untuk-menunjang-kesuksesan-bisnis</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/tren-digital-marketing-2022-yang-perlu-kamu-ketahui</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pengaruh-konsep-gamifikasi-terhadap-engagement-dan-minat-pelanggan</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/alasan-harus-perhatikan-logo-sebelum-membangun-sebuah-brand</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/solusi-membalas-chat-konsumen-cepat-dan-mudah-melalui-integrasi-chat-marketplace</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-pemasaran-untuk-meningkatkan-brand-awareness-melalui-gamification</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=2</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=3</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=4</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=5</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=6</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=7</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=8</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.64</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/5-hal-yang-perlu-diperhatikan-sebelum-menjual-produk-di-marketplace</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/ecommerce-enabler-solusi-untuk-kembangkan-bisnis-online-dengan-mudah</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/manfaat-menggunakan-search-engine-optimization-seo-dalam-bisnis-online</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/meraih-untung-maksimal-bisnis-online-dengan-seo</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-sistem-manajemen-gudang-bagi-pelaku-usaha</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-netflix-memahami-kebutuhan-penggunanya</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-orderan-datang-tepat-waktu-demi-menjamin-kepuasan-pelanggan</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-mengoptimalkan-manajemen-gudang-dalam-dunia-bisnis</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/5-tips-manajemen-stok-barang-dalam-bisnis-online</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/blogs?page=1</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-memaksimalkan-performa-toko-online-melalui-integrasi-chat</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/solusi-dalam-menyelesaikan-kendala-kelola-gudang</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-aplikasi-integrasi-marketplace-dapat-membantu-para-pebisnis-online</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/peranan-skill-management-software-menghadapi-tantangan-karir-di-tahun-2022</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-manajemen-stok-gudang-untuk-meminimalisir-kesalahan-pada-jumlah-barang</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-visualisasi-dan-analitik-data-membantu-brand-memahami-perilaku-konsumen</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/solusi-omnichannel-integrasi-marketplace-dan-multiplatform-untuk-integrasi-data</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-warehouse-management-system-menjadi-kunci-untuk-ekspansi-bisnis-online</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-brand-melakukan-personalisasi-dan-memenangkan-trust-konsumen-dengan-strategi-omnichannel</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/peranan-teknologi-dan-integrasi-marketplace-melalui-ecommerce-enabler-bagi-brand-dan-bisnis</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-teknologi-menjadikan-para-petani-sebagai-kolam-pasar-b2b</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-strategi-omnichannel-d2c-memberikan-kendali-atas-data-dan-consumer-journey-bagi-brand</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-omnichannel-pepsico-memanfaatkan-d2c-untuk-menanggapi-perubahan-perilaku-konsumen</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-digital-branding-danone-aqua-merebut-pasar-melalui-csr</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-d2c-nestle-melahirkan-nespresso-untuk-memenangkan-pasar-digital</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-netflix-menggunakan-data-untuk-menaikkan-jumlah-audiens-dan-sales</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-digital-omnichannel-marketing-berbasis-data-mystarbucks-card</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-omnichannel-marketing-yang-efektif-bagi-brand</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/kunci-utama-landing-page-memiliki-tingkat-konversi-tinggi</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-hyperlocal-ikea-memenangkan-persaingan-ritel-indonesia</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-gamifikasi-dominos-pizza-untuk-memahami-motivasi-konsumen-berdampak-pada-kenaikan-penjualan-30</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/kunci-utama-kesuksesan-strategi-digital-coca-cola-memenangkan-loyalitas-konsumen</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/cloud-warehouse-sebagai-solusi-integrasi-marketplace-dan-fulfilment-yang-customer-centric</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-asics-mampu-scale-up-di-pasar-digital-yang-disruptif</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/mengubah-proses-retur-barang-yang-kurang-menyenangkan-menjadi-strategi-retensi-pelanggan</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/cara-dhl-memimpin-pasar-logistik-dengan-merampingkan-operasional-fulfillmentnya</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-starbucks-menerapkan-gamification-melalui-kartu-digital-my-starbucks-reward</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/cara-mengoptimalkan-strategi-leads-conversion-untuk-menghasilkan-leads-berkualitas</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-nike-memanfaatkan-gamification-untuk-meningkatkan-brand-engagement-dan-loyalitas-penggunanya</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/perubahan-perilaku-konsumen-secara-digital-yang-perlu-dipahami-oleh-bisnis</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/mengukur-performa-bisnis-anda-secara-menyeluruh-dengan-balanced-scorecards</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/disrupsi-dan-pergeseran-tren-digitalisasi-logistik-dan-fulfilment-di-lanskap-industri-indonesia</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/menghitung-roi-dari-investasi-penggunaan-teknologi-dan-transformasi-digital-bagi-bisnis</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/sukses-digital-marketing-dengan-memanfaatkan-linkedin</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-fomo-berfungsi-sebagai-strategi-leads-conversion-dalam-digital-marketing</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/karakter-konsumen-indonesia-yang-mobile-first-dan-digital</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/peranan-transformasi-digital-dalam-industri-yang-disruptif</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/pentingnya-metriks-dalam-digital-marketing</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/3-fokus-utama-dalam-digital-branding</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-gamifikasi-untuk-membangun-brand-engagement</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/peranan-teknologi-dalam-scale-up-bisnis-di-era-digital</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/menerjemahkan-filosofi-board-game-kuno-asal-asia-timur-ke-dalam-machine-learning-google</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/peranan-digital-transformation-dalam-menghubungkan-antara-petani-konsumen-hingga-investor</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/rahasia-dibalik-product-development-traveloka</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-menurunkan-cost-per-lead-dengan-memanfaatkan-data</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-strategi-untuk-bisa-melakukan-digital-transformation-bisnis-tanpa-berinvestasi-tinggi-dalam-teknologi</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/alasan-mengapa-bisnis-perlu-go-digital-sebelum-terlambat</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-nike-memenangkan-pasar-audiens-digital-lewat-membangun-komunitas</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/bagaimana-adidas-dan-lulu-lemon-memanfaatkan-dtc-sebagai-strategi-digital-commerce-yang-diandalkan</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/strategi-customer-retention-yang-berangkat-dari-keluhan-konsumen-saat-harus-terjadi-retur-barang</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/membangun-loyalitas-pelanggan-dengan-implementasi-gamification</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>
<url>
  <loc>https://yokesen.com/id/detail-blog/merebut-hati-konsumen-dengan-pengiriman-lebih-cepat-kemanapun</loc>
  <lastmod>2022-02-02T04:15:59+00:00</lastmod>
  <priority>0.51</priority>
</url>


</urlset>