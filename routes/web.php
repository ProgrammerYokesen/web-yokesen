<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function () {

    Route::get('/', 'PageController@homePage')->name('homePage');
    Route::get('/company', 'PageController@aboutPage')->name('aboutPage');
    Route::get('/our-services', 'PageController@digitalPage')->name('digitalPage');
    Route::get('/that-to-this', 'PageController@thisThatPage')->name('thisThatPage');
    Route::get('/contact-us', 'PageController@contactPage')->name('contactPage');
    Route::get('/our-client', 'PageController@portoPage')->name('portoPage');
    Route::get('/gamification', 'PageController@gamificationPage')->name('gamificationPage');
    Route::get('/webinar', 'PageController@webinarPage')->name('webinarPage');


    Route::get('/blogs', 'blogController@blogPage')->name('blogPage');
    Route::get('/detail-blog/{slug}', 'blogController@detailBlogPage')->name('detailBlogPage');
    // Route::post('/like-blog', 'blogController@like')->name('blogLike');
    // Route::post('/comment-blog', 'blogController@comment')->name('blogComment');
    // Route::post('/load-comment', 'blogController@loadMoreComment')->name('loadMoreComment');
    Route::post('/register', 'RegisterController@register')->name('registerData');
    Route::get('/sitemap', 'PageController@sitemapPage')->name('sitemapPage');
    Route::get('/sitemap.xml', 'PageController@sitemapXMLPage')->name('sitemapXMLPage');

    // Landing Page Route
    Route::get('/teratur-cloud-warehouse', 'PageController@teraturLandingPage')->middleware('saveFootprint')->name('teraturLandingPage');

    /**Route Admin */
    Route::get('admin', 'DashboardControllerz@getIndex')->name('dashboard');

    // Users Detail
    Route::get('admin/leads/detail/{id}', 'UserController@userDetail')->name('userLeadDetail');
    Route::get('admin/contacts/detail/{id}', 'UserController@userDetail')->name('userContactDetail');
    Route::get('admin/potentials/detail/{id}', 'UserController@userDetail')->name('userPotentialDetail');
    Route::get('admin/wins/detail/{id}', 'UserController@userDetail')->name('userWinDetail');
    Route::get('admin/loses/detail/{id}', 'UserController@userDetail')->name('userLoseDetail');

    // Users Action
    Route::put('admin/{id}/update-status', 'UserController@updateStatus')->name('updateStatus');
    Route::post('admin/post-activity', 'ProcessController@postActivity')->name('postActivity');
    Route::post('admin/follow-up', 'ProcessController@processFollowUp')->name('processFollowUp');

    // New Event
    Route::get('/teknologi-dan-strategi-digital', 'NewLandingController@eventLP')->name('eventLP');
    // Route::get('/public', 'PageController@homePage')->name('homePage');

    Route::any('/public', function () {
        abort(404);
    });
    Route::put('admin/assign-product', 'ProcessController@assignUser')->name('assignUser');

});
    Route::get('/meeting', function () {
        return redirect('https://us02web.zoom.us/j/8013581311?pwd=dTlaYXcwYUxYa085eG1NUDZzK1RyQT09');
    });
Route::put('admin/assign-product', 'ProcessController@assignUser')->name('assignUser');





// Resonansi
// Route::get('/campaign-resonansi', 'ResonansiController@resonansiHome')->name('resonansiHome');

Route::post('meet/create', 'NewLandingController@createMeet')->name('meeting-create');
Route::post('contactus/craete', 'ContactUsController@create')->name('contacUs-create');

Route::get('admin/custom', 'DashboardController@getIndex')->name('dashboard-custom');
Route::get('admin', 'DashboardController@getIndex')->name('dashboard');
// Route::get('admin', [ 'as' => 'admin', 'uses' => 'DashboardController@getIndex']);
Route::name('dashboard')->get('admin', 'DashboardController@getIndex');
// Route::get('/admin', [
//     'as' => 'dashboard',
//     'uses' => 'DashboardController@getIndex'
// ]);
Route::get('hallo', function(){
    echo "Ok";
})->name('hallo');
Route::get('/coba', function(){
    echo "Ok";
})->name('ok-tess');